package com.MEDIS.repository;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnrollmentDb extends JpaRepository<EnrollmentModel, EnrollmentId> {
    EnrollmentModel findByUserAndSeminar(UserModel user, SeminarModel seminar);
}
