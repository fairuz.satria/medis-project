package com.MEDIS.repository;

import com.MEDIS.model.user.InvitedSpeakerModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InvitedSpeakerDb extends JpaRepository<InvitedSpeakerModel, String> {
    @Override
    Optional<InvitedSpeakerModel> findById(String s);

    Optional<InvitedSpeakerModel> findByEmail(String email);
}
