package com.MEDIS.repository;

import com.MEDIS.model.Id.SertifikatId;
import com.MEDIS.model.webinar.SertifikatModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SertifikatDb extends JpaRepository<SertifikatModel, SertifikatId> {
}
