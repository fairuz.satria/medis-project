package com.MEDIS.repository;

import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.model.webinar.SeminarModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SeminarDb extends JpaRepository<SeminarModel, String>{
    Optional<SeminarModel> findBySeminarId(String seminarId);
    List<SeminarModel> findByIsRecordedAndTitleContaining(Boolean isRecorded, String searchBy);

    Page<SeminarModel> findByIsRecordedAndTitleContaining(Boolean isRecorded, String searchBy, Pageable page);

//    List<SeminarModel> findByIsRecordedAndNamaSeminarContaining(Boolean isRecorded, String searchBy);
    List<SeminarModel> findByIsRecorded(Boolean isRecorded);
    List<SeminarModel> findAll();

    List<SeminarModel> findBySeminarIdContains(String s);

    List<SeminarModel> findSeminarModelByPenyelenggara(PenyelenggaraModel penyelenggara);
}
