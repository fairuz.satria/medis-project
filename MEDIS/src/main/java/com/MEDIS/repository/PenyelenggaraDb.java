package com.MEDIS.repository;

import com.MEDIS.model.user.PenyelenggaraModel;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import javax.swing.text.html.Option;
import java.util.Map;

public interface PenyelenggaraDb extends JpaRepository<PenyelenggaraModel, String> {
    Optional<PenyelenggaraModel> findByEmail(String username);

}
