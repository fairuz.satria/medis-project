package com.MEDIS.repository;

import com.MEDIS.model.request.RequestInvitedSpeaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestInvitedSpeakerDb extends JpaRepository<RequestInvitedSpeaker, String> {
    List<RequestInvitedSpeaker> findByStatus(String status);
}
