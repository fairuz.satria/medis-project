package com.MEDIS.repository;

import com.MEDIS.model.BannedStatusModel;
import com.MEDIS.model.Id.BannedStatusId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannedStatusDb extends JpaRepository<BannedStatusModel, BannedStatusId> {
}
