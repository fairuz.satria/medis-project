package com.MEDIS.repository;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RequestPenyelenggaraDb extends JpaRepository<RequestPenyelenggaraModel, String>{
    List<RequestPenyelenggaraModel> findByStatus(String requestStatus);
    Optional<RequestPenyelenggaraModel> findByRequestId(String requestId);
}
