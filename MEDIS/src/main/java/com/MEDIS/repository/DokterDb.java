package com.MEDIS.repository;

import com.MEDIS.model.user.DokterModel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Transactional
public interface DokterDb extends JpaRepository<DokterModel, String> {
    DokterModel findByEmail(String email);
    // Optional<DokterModel> findByEmail(String email);

    @Modifying
    @Query("update DokterModel d set d.email = :email, d.nama = :nama, d.tanggalLahir = :tanggalLahir, d.jenisKelamin = :jenisKelamin, d.photoUrl = :photoUrl where d.uuid = :uuid")
    void updateDokterDataDiri(@Param(value = "uuid") String uuid, @Param(value = "email") String email, @Param(value = "nama") String nama, @Param(value="tanggalLahir")LocalDate tanggalLahir, @Param(value="jenisKelamin") String jenisKelamin, @Param(value="photoUrl") String photoUrl);

    @Modifying
    @Query("update DokterModel d set d.spesialisasi = :spesialisasi, d.institusi = :institusi, d.kotaKabupaten = :kotaKabupaten where d.uuid = :uuid")
    void updateDokterDataPekerjaan(@Param(value = "uuid") String uuid, @Param(value = "spesialisasi") String spesialisai, @Param(value = "institusi") String institusi, @Param(value = "kotaKabupaten") String kotaKabupaten);

    List<DokterModel> findByNamaContaining(String searchBy);
}


