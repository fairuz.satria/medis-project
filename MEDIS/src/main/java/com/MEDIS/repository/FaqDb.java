package com.MEDIS.repository;

import com.MEDIS.model.FaqModel;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface FaqDb extends JpaRepository<FaqModel, String> {
    List<FaqModel> findByQuestionContaining(String searchBy);
}
