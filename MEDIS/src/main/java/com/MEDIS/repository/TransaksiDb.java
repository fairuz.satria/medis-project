package com.MEDIS.repository;

import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Map;


@Repository
public interface TransaksiDb extends JpaRepository<TransaksiModel, String>{
    List<TransaksiModel> findByUserAndSeminar(UserModel user, SeminarModel seminar);

    List<TransaksiModel> findAllByUser(UserModel user);

    TransaksiModel findByOrderId(String orderId);

    List<TransaksiModel> findAllByUserAndStatusTransaksi(UserModel user, String status);
}


