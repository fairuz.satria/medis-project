package com.MEDIS.payload;

import com.MEDIS.model.user.DokterModel;
import lombok.Data;

import java.util.List;

@Data
public class InviteDokterResponse {
    private List<DokterModel> listDokter;
}
