package com.MEDIS.controller;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.InvitedSpeakerModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.EnrollmentService;
import com.MEDIS.service.InvitedSpeakerService;
import com.MEDIS.service.SeminarService;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.service.UserService;
import com.MEDIS.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class InvitedSpeakerCotroller {
    @Autowired
    private InvitedSpeakerService invitedSpeakerService;

    @Autowired
    SeminarService seminarService;

    @Autowired
    UserService userService;

    @Autowired
    EnrollmentService enrollmentService;

    @GetMapping("/invited-speaker/dashboard/list-invited-webinar")
    public String getInvitedWebinarLayout(Model model){
        return "read-list-invited-webinar";
    }

    @GetMapping("/invited-speaker/dashboard/list-invited-webinar/table-body-invited-webinar.html")
    public String getInvitedWebinarTableBody(@RequestParam(required = true) String status, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        List<EnrollmentModel> listEnrollment = invitedSpeakerService.getInvitedWebinar(username, status);

        model.addAttribute("listEnrollment", listEnrollment);
        return "fragments/table-body-invited-webinar";
    }

    @GetMapping("/invited-speaker/dashboard/ubah-password")
    public String ubahPassword(Model model){
        return "form-ubah-password-inv-speaker";
    }

    @PostMapping("/invited-speaker/dashboard/ubah-password")
    public ModelAndView ubahPasswordPost(@RequestParam String oldPassword,
                                         @RequestParam String newPassword, Model model){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        UserModel userInvitedSpeaker = userService.getUserByEmail(username);

        if(UserServiceImpl.passwordMatches(userInvitedSpeaker.getPassword(), oldPassword)){
            userInvitedSpeaker.setPassword(newPassword);
            userService.addUser(userInvitedSpeaker);
            return new ModelAndView("redirect:/invited-speaker/dashboard/list-invited-webinar");
        }
        return new ModelAndView("redirect:/error");
    }



    @GetMapping("/invited-speaker/dashboard/list-invited-webinar/detail/{seminarId}")
    public String detailInvitedWebinar(Model model, @PathVariable String seminarId){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String uuid = user.getUuid();

        EnrollmentId enrollmentId = new EnrollmentId(uuid, seminarId);

        EnrollmentModel enrollment = enrollmentService.getEnrollmentById(enrollmentId);
        if(enrollment != null){
            model.addAttribute("enrollment", enrollment);
            return "detail-invited-webinar";
        }
        return "fail";
    }

}
