package com.MEDIS.controller;

import com.MEDIS.model.BannedStatusModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.payload.EmailDetailsPayload;
import com.MEDIS.service.BannedStatusService;
import com.MEDIS.service.DokterService;
import com.MEDIS.service.UserService;
import com.MEDIS.service.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    DokterService dokterService;

    @Autowired
    BannedStatusService bannedStatusService;

    @Autowired
    EmailService emailService;

    @GetMapping("/forgot-password")
    public String forgotPassword(){
        return "form-lupa-password";
    }

    @PostMapping("/forgot-password")
    public ModelAndView forgotPasswordSubmit(@RequestParam("email") String email, Model model, HttpServletRequest request){
        UserModel user = userService.getUserByEmail(email);
        if(user != null){
            user.setForgotUrl("generate");
            userService.addUser(user);

            String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
                    .replacePath(null)
                    .build()
                    .toUriString();

            EmailDetailsPayload emailPayload = new EmailDetailsPayload();
            emailPayload.setSubject("Reset password confirmation");
            emailPayload.setRecipient(email);
            emailPayload.setMsgBody("please click the link bellow to reset your password.\n" +
                     baseUrl + "/forgot-password/reset-password/" + user.getForgotUrl() +"\n" +
                    "please don't share the link to anyone for your own safety\n" +
                    "\n" +
                    "feel free to contact us at:\n" +
                    "test@gmail.com");
            emailService.sendSimpleMail(emailPayload);

            model.addAttribute("message", "Check your email for a link to reset your password. Check your spam folder too it should" +
                    " be appear in a few minutes");
            return new ModelAndView("redirect:/login");
        }else{
            model.addAttribute("messages", "email tidak ditemukan");
            return new ModelAndView("redirect:/error");
        }
    }

    @GetMapping("/forgot-password/reset-password/{forgotUrl}")
    public String resetPassword(@PathVariable String forgotUrl, Model model){
        UserModel user = userService.getUserByForgotUrl(forgotUrl);
        if(user == null){
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
        }
        model.addAttribute("user", user);
        return "form-reset-password";
    }

    @PostMapping("/forgot-password/reset-password/{forgotUrl}")
    public ModelAndView resetPasswordSubmit(@PathVariable String forgotUrl, Model model, @RequestParam String newPassword){
        UserModel user = userService.getUserByForgotUrl(forgotUrl);
        if(user != null){
                user.setPassword(newPassword);
                user.setForgotUrl("null");
                userService.addUser(user);
                return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("");
    }

    @GetMapping("/admin/dashboard/pengelolaan-akun")
    public String getPengelolaanAkunLayout(Model model){
        return "read-list-pengelolaan-akun";
    }

    @GetMapping("/admin/dashboard/pengelolaan-akun/table-body-pengelolaan-akun.html")
    public  String getPengelolaanAkunTableBody(@RequestParam(required = true) String status,@RequestParam(required = true) String role, @RequestParam(required = false) String search, Model model){
        List<UserModel> listUser = null;
        search = search==null?"":search;
        if(status.equals("All")){
            if(role.equals("All")){
                listUser = userService.getUserBySearch(search);
            }else{
                listUser = userService.getUserByRoleSearch(role, search);
            }
        }else{
            if(role.equals("All")){
                listUser = userService.getUserByBannedStatusSearch(status, search);
            }else {
                listUser = userService.getUserByBannedStatusRoleSearch(status, role, search);
            }
        }
        model.addAttribute("listUser", listUser);
        return "fragments/table-body-pengelolaan-akun";
    }

    @GetMapping("/admin/dashboard/pengelolaan-akun/detail/{uuid}")
    public String detailUser(Model model, @PathVariable String uuid){
        UserModel user = userService.getUserByUuid(uuid);
        if(user != null){
            model.addAttribute("user", user);
            return "detail-pengelolaan-akun";
        }
        return "fail";
    }

    @PostMapping("/admin/dashboard/pengelolaan-akun/detail/{uuid}")
    public ModelAndView detailUserUnban(Model model, @PathVariable String uuid){
        UserModel user = userService.getUserByUuid(uuid);
        BannedStatusModel bannedStatusModel = user.getBannedStatusModel();
        if(user != null){

            bannedStatusModel.setUser(null);
            bannedStatusModel = bannedStatusService.saveBannedStatusModel(bannedStatusModel);
            System.out.println(bannedStatusModel.getUser());

            user.setBannedStatusModel(null);;
            userService.saveUserNoEncrpyt(user);

            model.addAttribute("user", user);
            return new ModelAndView("redirect:/admin/dashboard/pengelolaan-akun/detail/"+uuid);
        }
        return new ModelAndView("/fail");
    }

    @GetMapping(value = "/admin/dashboard/pengelolaan-akun/detail/{uuid}/ban-akun")
    public String banAkunForm(Model model, @PathVariable String uuid){
            model.addAttribute("uuid", uuid);
            return "form-ban-akun";
    }

    @PostMapping(value = "/admin/dashboard/pengelolaan-akun/detail/{uuid}/ban-akun")
    public ModelAndView banAkunPost(@ModelAttribute BannedStatusModel bannedStatusModel, @PathVariable String uuid, Model model ){
        UserModel user = userService.getUserByUuid(uuid);
        if(user != null){
            bannedStatusModel.setUser(user);
            bannedStatusModel = bannedStatusService.saveBannedStatusModel(bannedStatusModel);

            user.setBannedStatusModel(bannedStatusModel);
            userService.saveUserNoEncrpyt(user);
            return new ModelAndView("redirect:/admin/dashboard/pengelolaan-akun/detail/"+uuid);
        }
        return new ModelAndView("fail");
    }
}
