package com.MEDIS.controller;

import com.MEDIS.model.FaqModel;
import com.MEDIS.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FaqController {
    @Autowired
    FaqService faqService;

    @GetMapping("/faq")
    public String getAllFaqView(Model model){
        return "view-all-faq";
    }
    @GetMapping("/faq/view-body-all-faq.html")
    public String getAllFaqViewBody(Model model, @RequestParam(required = false) String searchBy){
        searchBy = searchBy==null?"":searchBy;

        model.addAttribute("listFaq", faqService.getAllFaqSearchBy(searchBy));
        return "fragments/view-body-all-faq";
    }
    @GetMapping("/admin/dashboard/pengelolaan-faq/table-body-pengelolaan-faq.html")
    public String getAllFaqPengelolaanTableBody(Model model){
        model.addAttribute("listFaq", faqService.getAllFaq());
        return "fragments/table-body-pengelolaan-faq";
    }

    @GetMapping("/admin/dashboard/pengelolaan-faq")
    public String getAllFaqPengelolaan(Model model){
        return "read-list-pengelolaan-faq";
    }

    @GetMapping("/admin/dashboard/pengelolaan-faq/add")
    public String getCreateFaqPengelolaan(Model model){
        return "form-add-faq";
    }

    @GetMapping("/admin/dashboard/pengelolaan-faq/{faqId}/update")
    public String getUpdateFaqPengelolaan(Model model, @PathVariable String faqId){
        FaqModel faq = faqService.getByFaqId(faqId);
        model.addAttribute("faq", faq);
        return "form-update-faq";
    }

    @PostMapping("/admin/dashboard/pengelolaan-faq/delete")
    public ModelAndView postDeleteFaqPengelolaan(Model model, @ModelAttribute FaqModel faq){
        faqService.deleteFaq(faq);
        return new ModelAndView("redirect:/admin/dashboard/pengelolaan-faq");
    }

    @PostMapping("/admin/dashboard/pengelolaan-faq/save")
    public ModelAndView postCreateFaqPengelolaan(Model model, @ModelAttribute FaqModel faq){
        faqService.save(faq);
        return new ModelAndView("redirect:/admin/dashboard/pengelolaan-faq");
    }


}
