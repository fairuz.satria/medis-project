package com.MEDIS.controller;

import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;
import com.MEDIS.service.DokterService;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.TransaksiService;
import com.MEDIS.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
//@RequestMapping("/transaksi")
public class TransaksiController {

    @Autowired
    TransaksiService transaksiService;

    @Autowired
    UserService userService;

    @Autowired
    DokterService dokterService;

    @Autowired
    SeminarService seminarService;

    @GetMapping("/dokter/dashboard/transactions")
    public String listTransaksi(Model model, Principal principal, @RequestParam(required = false) String status) {
        UserModel user = userService.getUserByEmail(principal.getName());
        DokterModel dokter = dokterService.getByEmail(principal.getName());


        List<TransaksiModel> listTransaksi;

        if (status == null) {
            listTransaksi = transaksiService.getListTransaksiByUser(user);
        } else {
            listTransaksi = transaksiService.getListTransaksiByUserAndStatusTransaksi(user, status);
        }

        List<SeminarModel> listSeminar = transaksiService.getListSeminarFromListTransaksi(listTransaksi);

        model.addAttribute("user", dokter);
        model.addAttribute("listTransaksi", listTransaksi);
        model.addAttribute("listSeminar", listSeminar);
        model.addAttribute("filterStatus", status==null?"":status);
        return "transaksi/list-history-pembayaran-seminar";
    }

    @GetMapping("/dokter/dashboard/transactions/{id}")
    public String detailTransaksi(@PathVariable("id") String id, Model model, Principal principal) {
        UserModel user = userService.getUserByEmail(principal.getName());
        TransaksiModel transaksi = transaksiService.getTransaksiByTransaksiId(id);
        SeminarModel seminar = transaksi.getSeminar();

        model.addAttribute("user", user);
        model.addAttribute("transaksi", transaksi);
        model.addAttribute("seminar", seminar);
        return "transaksi/detail-invoice";
    }

    @GetMapping("/transactions/paid/{id}")
    public String detailAcceptedPayment(@PathVariable("id") String id, Model model, Principal principal) {
        UserModel user = userService.getUserByEmail(principal.getName());
        TransaksiModel transaksi = transaksiService.getTransaksiByTransaksiId(id);
        SeminarModel seminar = transaksi.getSeminar();

        model.addAttribute("user", user);
        model.addAttribute("seminar", seminar);
        model.addAttribute("transaksi", transaksi);
        return "transaksi/detail-pembayaran-webinar-berhasil";
    }
}
