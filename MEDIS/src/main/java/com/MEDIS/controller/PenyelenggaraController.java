package com.MEDIS.controller;

import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.UserService;
import com.MEDIS.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PenyelenggaraController {
    @Autowired
    SeminarService seminarService;

    @Autowired
    UserService userService;

    @GetMapping("/penyelenggara/dashboard/history-pembuatan-webinar")
    public String getHistoryPembuatanWebinarLayout(Model model) {
        return "read-list-created-webinar";
    }
    @GetMapping("/penyelenggara/dashboard/history-pembuatan-webinar/table-body-pembuatan-webinar.html")
    public String getHistoryPembuatanWebinarTableBody(@RequestParam String status, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String email = user.getUsername();

        PenyelenggaraModel penyelenggara = new PenyelenggaraModel();
        penyelenggara.setEmail(email);
        penyelenggara.setUuid(user.getUuid());

        List<SeminarModel> listSeminar = seminarService.getHistoryWebinarPenyelenggaraByStatus(penyelenggara, status);
        model.addAttribute("listSeminar", listSeminar);
        return "fragments/table-body-pembuatan-webinar";
    }

    @GetMapping("/penyelenggara/dashboard/history-pembuatan-webinar/detail/{seminarId}")
    public String getDetailPembuatanWebinar(Model model, @PathVariable String seminarId){
        model.addAttribute("seminar", seminarService.findBySeminarId(seminarId));
        return "detail-pembuatan-webinar";
    }

    @GetMapping("/penyelenggara/dashboard/ubah-password")
    public String ubahPassword(Model model){
        return "form-ubah-password-penyelenggara";
    }

    @PostMapping("/penyelenggara/dashboard/ubah-password")
    public ModelAndView ubahPasswordPost(@RequestParam String oldPassword,
                                         @RequestParam String newPassword, Model model){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        UserModel userInvitedSpeaker = userService.getUserByEmail(username);

        if(UserServiceImpl.passwordMatches(userInvitedSpeaker.getPassword(), oldPassword)){
            userInvitedSpeaker.setPassword(newPassword);
            userService.addUser(userInvitedSpeaker);
            return new ModelAndView("redirect:/penyelenggara/dashboard/history-pembuatan-webinar");
        }
        return new ModelAndView("redirect:/error");
    }
}
