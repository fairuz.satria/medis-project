package com.MEDIS.controller;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.SertifikatModel;
import com.MEDIS.service.DokterService;
import com.MEDIS.service.EnrollmentService;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.SertifikatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;

@Controller
class SertifikatController {
    @Autowired
    SertifikatService sertifikatService;

    @Autowired
    DokterService dokterService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    SeminarService seminarService;

    @GetMapping("/sertifikat/{userId}/{seminarId}")
    public String getOrGenerateSertifikat(Model model, @PathVariable String userId,  @PathVariable String seminarId) throws IOException {
        EnrollmentModel enrollment = enrollmentService.getEnrollmentById(new EnrollmentId(userId, seminarId));
        SeminarModel seminar = enrollment.getSeminar();
        UserModel user = enrollment.getUser();

        SertifikatModel sertifikat = enrollment.getSertifikat();
        if(sertifikat != null){
            model.addAttribute("sertifikat", sertifikat);
            model.addAttribute("enrollment", enrollment);
            return "sertifikat/sertifikat";
        }else{
            if(seminar.getCertificateUrl() != null && seminar.getStatus().equals("Done")){
                sertifikat = sertifikatService.generateSertifikat(enrollment, seminar, user);
                model.addAttribute("sertifikat", sertifikat);
                model.addAttribute("enrollment", enrollment);
                return "sertifikat/sertifikat";
            }
            return "fail";
        }
    }
}