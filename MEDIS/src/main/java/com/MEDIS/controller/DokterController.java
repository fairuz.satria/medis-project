package com.MEDIS.controller;


import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.*;
import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class DokterController {

    @Autowired
    DokterService dokterService;

    @Autowired
    UserService userService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    StorageService storageService;

    @GetMapping("/signup")
    public String addDokterFormPage(Model model){
//        DokterModel dokter = new DokterModel();
//        model.addAttribute("dokter", dokter);
        // model.addAttribute("user", userLoggedIn);
        return "sign-up";
    }

    @PostMapping("/signup")
    public ModelAndView addDokterSubmitPage(@ModelAttribute DokterModel dokter, Model model){
        // DokterModel userLoggedIn = adminService.getAdminLoggedIn();
        // dokter.setRole("Dokter");
        // dokter.setIsSso(false);
        dokter.setRole("DOKTER");
        dokterService.addDokter(dokter);

        // logger.info("Created Dokter {}", dokter.getNama());
        model.addAttribute("dokter", dokter);
        // model.addAttribute("user", userLoggedIn);
        return new ModelAndView("redirect:/login");
    }

    @GetMapping("/dokter/dashboard/history-webinar")
    public String getHistoryWebinarLayout(Model model) {
        return "read-list-pendaftaran-webinar";
    }
    @GetMapping("/dokter/dashboard/history-webinar/table-body-pendaftaran-webinar.html")
    public String getHistoryWebinarTableBody(@RequestParam String status,Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        List<EnrollmentModel> listEnrollment = userService.getHistoryWebinarByStatus(username, status);
        model.addAttribute("listEnrollment", listEnrollment);
        return "fragments/table-body-pendaftaran-webinar";
    }
    @GetMapping("/dokter/profil/{email}")
    public String getProfilOtherDokter(@PathVariable String email, Model model){
        DokterModel dokter = dokterService.getByEmail(email);

        model.addAttribute("user", dokter);
        return "detail-akun-dokter";
    }

    @GetMapping("/dokter/dashboard/profil")
    public String getProfilDokter(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        DokterModel dokter = dokterService.getByEmail(username);
        model.addAttribute("user", dokter);
        return "detail-akun-dokter";



    }

    @GetMapping("/dokter/dashboard/history-webinar/detail/{seminarId}")
    public String detailHistoryWebinar(Model model, @PathVariable String seminarId){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String uuid = user.getUuid();

        EnrollmentId enrollmentId = new EnrollmentId(uuid, seminarId);

        EnrollmentModel enrollment = enrollmentService.getEnrollmentById(enrollmentId);
        if(enrollment != null){
            model.addAttribute("enrollment", enrollment);
            return "detail-pendaftaran-webinar";
        }
        return "fail";

    }

    @GetMapping("/dokter/dashboard/profil/update")
    public String updateProfilDokter(Model model){
        return "update-profile";
    }

    @GetMapping("/dokter/dashboard/profil/update/data-diri")
    public String updateDataDiriDokterGet(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        DokterModel dokter = dokterService.getByEmail(username);
        model.addAttribute("user", dokter);
        return "form-update-profile-data-diri";
    }

    @PostMapping("/dokter/dashboard/profil/update/data-diri")
    public ModelAndView updateDataDiriDokterPost(
            @ModelAttribute DokterModel dokter,
            @RequestParam(required = false, name = "image") MultipartFile image,
            Principal principal) throws IOException
    {
        System.out.println(dokter.getUuid());
        System.out.println(dokter.getTanggalLahir());
        UserModel user = userService.getUserByUuid(dokter.getUuid());

        String photoUrl = storageService.uploadImageToCloudStorage(image, user);
        dokter.setPhotoUrl(photoUrl);

        dokterService.updateDokterDataDiri(dokter);
        return new ModelAndView("redirect:/dokter/dashboard/profil");
    }

    @GetMapping("/dokter/dashboard/profil/update/data-pekerjaan")
    public String updateDataPekerjaanDokterGet(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        DokterModel dokter = dokterService.getByEmail(username);
        model.addAttribute("user", dokter);
        return "form-update-profile-data-pekerjaan";
    }

    @PostMapping("/dokter/dashboard/profil/update/data-pekerjaan")
    public ModelAndView updateDataPekerjaanDokterPost(@ModelAttribute DokterModel dokter, Model model){
        dokterService.updateDokterDataPekerjaan(dokter);
        return new ModelAndView("redirect:/dokter/dashboard/profil");
    }

    @GetMapping("/dokter/dashboard/ubah-password")
    public String ubahPassword(Model model){
        return "form-ubah-password";
    }

    @PostMapping("/dokter/dashboard/ubah-password")
    public ModelAndView ubahPasswordPost(@RequestParam String oldPassword,
            @RequestParam String newPassword, Model model){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        String username = user.getUsername();

        UserModel userDokter = userService.getUserByEmail(username);

        if(UserServiceImpl.passwordMatches(userDokter.getPassword(), oldPassword)){
            userDokter.setPassword(newPassword);
            userService.addUser(userDokter);
            return new ModelAndView("redirect:/dokter/dashboard/profil");
        }
        return new ModelAndView("redirect:/error");
    }

}
