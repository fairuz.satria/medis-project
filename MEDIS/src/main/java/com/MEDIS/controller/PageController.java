package com.MEDIS.controller;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.payload.EmailDetailsPayload;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.UserService;
import com.MEDIS.service.email.EmailService;
import com.MEDIS.service.request.RequestPenyelenggaraService;
import com.google.auth.oauth2.GoogleCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PageController {

    @Autowired
    UserService userService;

    @Autowired private EmailService emailService;

    @Autowired
    RequestPenyelenggaraService requestService;

    @Autowired
    SeminarService seminarService;
    @GetMapping("/")
    public String home(Model model, @RequestParam(required = false) String process) throws IOException {
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
//        System.out.println(credentials);

        if(process==null){
            process = "base";
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof String) {
            // There is no authenticated user

            System.out.println("unauthenticated");

        } else {
            // There is an authenticated user
            System.out.println("authenticated");
            UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
            String username = user.getUsername();
        }
        List<SeminarModel> listSeminar = seminarService.getAllSeminarExamples();
        System.out.println(listSeminar.toString());
        model.addAttribute("listSeminar", listSeminar);

        return "home";
    }

    @PostMapping("/")
    public ModelAndView submitForm(@ModelAttribute RequestPenyelenggaraModel request, Model model){
        EmailDetailsPayload emailPayload = new EmailDetailsPayload();
        emailPayload.setSubject("Request Penyelenggara Confirmation");
        emailPayload.setRecipient(request.getEmail());
        emailPayload.setMsgBody("Thank you for your request, we will inform you further\n" +
                "\n" +
                "feel free to contact us at:\n" +
                "test@gmail.com");
        emailService.sendSimpleMail(emailPayload);
        requestService.saveRequest(request);
        return new ModelAndView("redirect:/?process=success");
    }

    @RequestMapping("/login")
    public String login(Model model){
        return "log-in";
    }


    @GetMapping("/terms-and-conditions")
    public String termsAndCondition() {
        return "terms-and-conditions";
    }

}