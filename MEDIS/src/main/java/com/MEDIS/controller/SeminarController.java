package com.MEDIS.controller;

import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;
import com.MEDIS.payload.InviteDokterResponse;
import com.MEDIS.service.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.SeminarService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Controller
@RequestMapping("/seminars")
public class SeminarController {

    @Autowired
    private Environment env;
    @Autowired
    ZoomService zoomService;

    @Autowired
    SeminarService seminarService;

    @Autowired
    DokterService dokterService;

    @Autowired
    SertifikatService sertifikatService;

    @Autowired
    UserService userService;

    @Autowired
    PenyelenggaraService penyelenggaraService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    TransaksiService transaksiService;

    @Autowired
    PaymentService paymentService;

    @Autowired
    StorageService storageService;

    @GetMapping("")
    public String listSeminar(
            @RequestParam(required = false) String type,
            Model model,
            Principal principal) {
        UserModel user = userService.getUserByEmail(principal.getName());
        model.addAttribute("searchBy", "");
        model.addAttribute("role", user.getRole());
        model.addAttribute("type", type);
        return "seminar/list-webinar";
    }

    @GetMapping("/{id}")
    public String detailSeminar(@PathVariable("id") String id, Model model, Principal principal) {
        try {
            UserModel user = userService.getUserByEmail(principal.getName());
            SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
            model.addAttribute("seminar", seminar);

            if (user.getRole().equals("PENYELENGGARA") && seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
                JSONObject zoomJson = zoomService.getZoomMeeting(seminar.getMeetingId());
                String startUrl = zoomJson.getString("start_url");
                model.addAttribute("startUrl", startUrl);
                return "seminar/detail-webinar-penyelenggara";
            }

            if (enrollmentService.isEnrolled(user, seminar)) {

//                EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);
//                if (seminar.getIsRecorded() && enrollment.getRate() == null) {
//                    rateWebinarFormPage(seminar.getSeminarId(), model);
//                }

                return "seminar/detail-webinar-set-daftar";
            }

            if (seminar.getTiketTerjual() >= seminar.getJumlahTiket()) {
                return "seminar/detail-webinar-tiket-habis";
            }

            // enrollment gaada harusnya
            // perlu cek ada transaksi yang aktif atau ngga
            // kalau ngga ada baru buat snapToken baru
            TransaksiModel transaksi = transaksiService.getActiveTransaksi(user, seminar);

            String activeProfile = env.getProperty("spring.profiles.active");
            if (activeProfile == null) activeProfile = env.getProperty("spring.profiles.default");
            System.out.println("SPRING PROFILES ACTIVE: " + activeProfile);
            boolean isProduction = activeProfile.equals("prod");

            String snapToken;
            if (transaksi == null) {
                String orderId = UUID.randomUUID().toString();
                LocalDateTime tokenExpireDate = LocalDateTime.now().plusDays(1);
                snapToken = paymentService.getSnapToken(orderId, seminar.getHarga(), user, seminar);
                transaksi = new TransaksiModel();
                transaksi.setOrderId(orderId);
                transaksi.setSnapToken(snapToken);
                transaksi.setStatusTransaksi("temporary");
                transaksi.setSeminar(seminar);
                transaksi.setUser(user);
                transaksi.setTokenExpireDate(tokenExpireDate);
                transaksiService.createTransaksi(transaksi);
            } else if (LocalDateTime.now().isAfter(transaksi.getTokenExpireDate())) {
                String orderId = UUID.randomUUID().toString();
                LocalDateTime tokenExpireDate = LocalDateTime.now().plusDays(1);
                snapToken = paymentService.getSnapToken(orderId, seminar.getHarga(), user, seminar);
                transaksi.setOrderId(orderId);
                transaksi.setSnapToken(snapToken);
                transaksi.setTokenExpireDate(tokenExpireDate);
                transaksiService.updateTransaksi(transaksi);
            }

            snapToken = transaksi.getSnapToken();

            System.out.println("Snap Token: " + snapToken);

            model.addAttribute("clientKey", paymentService.getClientKey());
            model.addAttribute("snapToken", snapToken);
            model.addAttribute("transaksi", transaksi);
            model.addAttribute("isProduction", isProduction);
            return "seminar/detail-webinar-seb-daftar";

        } catch (Exception e) {
            System.out.println(e);
            return "error/error";
        }
    }

    @GetMapping("/watch-recorded/{id}")
    public String watchRecordedWebinar(@PathVariable("id") String id, Model model, Principal principal){
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());
        EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);

        if (!user.getRole().equals("PENYELENGGARA") && enrollment == null) {
            model.addAttribute("message", "Belum Terdaftar");
            return "error/error-custom-message";
        }

        model.addAttribute("seminar", seminar);

        return "seminar/watch-recorded-webinar";
    }

    @GetMapping("/join-windowed/{id}")
    public String joinSeminarComponentSDK(@PathVariable("id") String id, @RequestParam(required = false) String zak, Model model, Principal principal) {
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());
        EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);

        if (!user.getRole().equals("PENYELENGGARA") && enrollment == null) {
            model.addAttribute("message", "Belum Terdaftar");
            return "error/error-custom-message";
        }

        String signature;

//        if (enrollment.getSignature() != null && enrollment.getSignatureExpireDate().isBefore(LocalDateTime.now().minusMinutes(1))) {
//            signature = enrollment.getSignature();
//        } else {
//            LocalDateTime signatureExpireDate = LocalDateTime.now().plusHours(12); // the same as exp in ZoomServiceImpl
//            if (user.getRole().equals("PENYELENGGARA") && seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
//                signature = zoomService.generateSignature(seminar.getMeetingId(), 1);
//            } else {
//                signature = zoomService.generateSignature(seminar.getMeetingId(), 0);
//            }
//            enrollment.setSignature(signature);
//            enrollment.setSignatureExpireDate(signatureExpireDate);
//            enrollmentService.updateEnrollment(enrollment);
//        }

        if (user.getRole().equals("PENYELENGGARA") && seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
            signature = zoomService.generateSignature(seminar.getMeetingId(), 1);
        } else {
            signature = zoomService.generateSignature(seminar.getMeetingId(), 0);
        }

        if (seminar.getDateTime().isAfter(LocalDateTime.now())) {
            model.addAttribute("leaveUrl", "/seminars/rate/" + seminar.getSeminarId());
        } else {
            model.addAttribute("leaveUrl", "/seminars/" + seminar.getSeminarId());
        }
        model.addAttribute("sdkKey", zoomService.getSdkKey());
        model.addAttribute("signature", signature);
        model.addAttribute("seminar", seminar);
        model.addAttribute("user", user);
        model.addAttribute("zak", zak==null?"":zak); // TODO: entah kenapa ga perlu zak, kalau ada bug start seminar check ini
        return "seminar/join-webinar-component";
    }

    @GetMapping("/join-fullscreen/{id}")
    public String joinSeminarClientSDK(@PathVariable("id") String id, @RequestParam(required = false) String zak, Model model, Principal principal) {
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());
        EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);
        String signature;

        if (!user.getRole().equals("PENYELENGGARA") && enrollment == null) {
            model.addAttribute("message", "Belum Terdaftar");
            return "error/error-custom-message";
        }

//        if (enrollment.getSignature() != null && LocalDateTime.now().isBefore(enrollment.getSignatureExpireDate().minusMinutes(1))) {
//            signature = enrollment.getSignature();
//        } else {
//            System.out.println("buat token baru");
//            LocalDateTime signatureExpireDate = LocalDateTime.now().plusHours(12); // the same as exp in ZoomServiceImpl
//            if (user.getRole().equals("PENYELENGGARA") && seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
//                signature = zoomService.generateSignature(seminar.getMeetingId(), 1);
//            } else {
//                signature = zoomService.generateSignature(seminar.getMeetingId(), 0);
//            }
//            enrollment.setSignature(signature);
//            enrollment.setSignatureExpireDate(signatureExpireDate);
//            enrollmentService.updateEnrollment(enrollment);
//        }

        if (user.getRole().equals("PENYELENGGARA") && seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
            signature = zoomService.generateSignature(seminar.getMeetingId(), 1);
        } else {
            signature = zoomService.generateSignature(seminar.getMeetingId(), 0);
        }

        if (seminar.getDateTime().isBefore(LocalDateTime.now())) {
            model.addAttribute("leaveUrl", "/seminars/rate/" + seminar.getSeminarId());
        } else {
            model.addAttribute("leaveUrl", "/seminars/" + seminar.getSeminarId());
        }
        model.addAttribute("sdkKey", zoomService.getSdkKey());
        model.addAttribute("signature", signature);
        model.addAttribute("seminar", seminar);
        model.addAttribute("user", user);
        model.addAttribute("zak", zak==null?"":zak); // TODO: entah kenapa ga perlu zak, kalau ada bug start seminar check ini
        return "seminar/join-webinar-client";
    }

    @GetMapping("/add")
    public String addSeminarFormPage(Model model, Principal principal) {
        SeminarModel seminar = new SeminarModel();
        model.addAttribute("seminar", seminar);

        PenyelenggaraModel penyelenggara = penyelenggaraService.findPenyelenggaraByEmail(principal.getName());
        int jumlahSeminar = penyelenggara.getListSeminar().size();
        if (jumlahSeminar >= penyelenggara.getKuotaPembuatan()) {
            model.addAttribute("message", "Kuota Pembuatan Webinar Sudah Habis");
            return "error/error-custom-message";
        }

        return "seminar/form-add-webinar";
    }

    @PostMapping("/add")
    public RedirectView addSeminarSubmitPage(
            @ModelAttribute SeminarModel seminar,
            @RequestParam(required = false, name = "fileEvent") MultipartFile fileEvent,
            @RequestParam(required = false, name = "image") MultipartFile image,
            Principal principal)
    {
        try {
            PenyelenggaraModel penyelenggara = penyelenggaraService.findPenyelenggaraByEmail(principal.getName());
            JSONObject seminarJson =  zoomService.createZoomMeeting(seminar);
            System.out.println(seminarJson);
            seminar.setPenyelenggara(penyelenggara);
            seminar.setSeminarId(seminarJson.getString("uuid").replace("/", "")); // mungkin perlu diganti
            seminar.setMeetingId(seminarJson.getString("id"));
            seminar.setJoinUrl(seminarJson.getString("join_url"));
            seminar.setPassword(seminarJson.getString("password"));
            seminar.setIsFinished(false);
            seminar.setIsRecorded(false);
            seminar.setTiketTerjual(0);

            String fileUrl = !Objects.equals(fileEvent.getOriginalFilename(), "") ? storageService.uploadPdfToCloudStorage(fileEvent, seminar) : null;
            String photoUrl = !Objects.equals(image.getOriginalFilename(), "") ?storageService.uploadImageToCloudStorage(image, seminar) : null;

            seminar.setFileUrl(fileUrl);
            seminar.setPhotoUrl(photoUrl);

            seminarService.createSeminar(seminar);

            List<SeminarModel> listSeminar = penyelenggara.getListSeminar();
            listSeminar.add(seminar);
            penyelenggara.setListSeminar(listSeminar);
            penyelenggaraService.savePenyelenggaraWithoutReq(penyelenggara);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new RedirectView("/seminars/" + seminar.getSeminarId());
    }

    @GetMapping("/edit-webinar/{id}")
    public String editWebinar(@PathVariable("id") String id, Model model) {
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        model.addAttribute("seminar", seminar);
        return "seminar/pilihan-update-webinar";
    }

    @GetMapping("/update/{id}")
    public String updateSeminarFormPage(@PathVariable("id") String id, Model model, Principal principal) {
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());

        if (!seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
            model.addAttribute("message", "Tidak Memiliki Akses");
            return "error/error-custom-message";
        }

        String fileUrl = seminar.getFileUrl()!=null ? seminar.getFileUrl() : "";
        String videoUrl = seminar.getVideoUrl()!=null ? seminar.getVideoUrl() : "";
        model.addAttribute("fileName", fileUrl.replace("https://storage.googleapis.com/medis_bucket/files/", "").replace("%20", " "));
        model.addAttribute("videoName", videoUrl.replace("https://storage.googleapis.com/medis_bucket/videos/", "").replace("%20", " "));
        model.addAttribute("seminar", seminar);
        return "seminar/form-update-webinar";
    }

    @PostMapping("/update")
    public RedirectView updateSeminarSubmitPage(
            @ModelAttribute SeminarModel newSeminar,
            @RequestParam(required = false, name = "fileEvent") MultipartFile fileEvent,
            @RequestParam(required = false, name = "image") MultipartFile image,
            @RequestParam(required = false, name = "video") MultipartFile video,
            Principal principal)
    {
        SeminarModel seminar = seminarService.getSeminarBySeminarId(newSeminar.getSeminarId());
        UserModel user = userService.getUserByEmail(principal.getName());

        if (!seminar.getPenyelenggara().getUuid().equals(user.getUuid())) {
            return new RedirectView("error");
        }

        if (!Objects.equals(video.getOriginalFilename(), "") && seminar.getVideoUrl() == null) {
            seminar.setIsRecorded(true);
        }

        try {
            String fileUrl = !Objects.equals(fileEvent.getOriginalFilename(), "") ? storageService.uploadPdfToCloudStorage(fileEvent, seminar) : seminar.getFileUrl();
            String photoUrl = !Objects.equals(image.getOriginalFilename(), "") ? storageService.uploadImageToCloudStorage(image, seminar) : seminar.getPhotoUrl();
            String videoUrl = !Objects.equals(video.getOriginalFilename(), "") ? storageService.uploadVideoToCloudStorage(video, seminar) : seminar.getVideoUrl();
            seminar.setFileUrl(fileUrl);
            seminar.setPhotoUrl(photoUrl);
            seminar.setVideoUrl(videoUrl);
        } catch (Exception e) {
            System.out.println(e);
        }

        seminar.setTitle(newSeminar.getTitle());
        seminar.setNamaPerusahaan(newSeminar.getNamaPerusahaan());
        seminar.setDateTime(newSeminar.getDateTime());
        seminar.setDuration(newSeminar.getDuration());
        seminar.setDeskripsi(newSeminar.getDeskripsi());
        seminarService.updateSeminar(seminar);

        return new RedirectView("/seminars/" + seminar.getSeminarId());
    }

    @GetMapping("/{role}/list-seminar/live/card-container.html")
    public String getCardContainer(
            Model model,
            @RequestParam(required = false) String searchBy,
            @RequestParam(required = false) String type,
            @RequestParam(required = false) Integer page)
    {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) auth.getPrincipal();
//        user.getAuthorities()

        searchBy = searchBy==null?"":searchBy;
        Boolean isRecorded = type.equals("recorded");

        Pageable pageable = PageRequest.of(page-1, 3);

        Page<SeminarModel> pageSeminar= seminarService.getAllSeminarIsRecordedSearchBy(isRecorded, searchBy, pageable);
        List<SeminarModel> listSeminar = pageSeminar.getContent();
        long totalElements = pageSeminar.getTotalElements();
        int totalPages = pageSeminar.getTotalPages();


        model.addAttribute("totalPages", totalPages);
        model.addAttribute("searchBy", searchBy);
        model.addAttribute("listSeminarLive", listSeminar);
        return "fragments/card-container";
    }

    @GetMapping("/footer.html")
    public String getFooter(
            Model model,
            @RequestParam(required = false) String searchBy,
            @RequestParam(required = false) String type,
            @RequestParam(required = false) Integer page
    ) {
        searchBy = searchBy==null?"":searchBy;
        Boolean isRecorded = type.equals("recorded");

        Pageable pageable = PageRequest.of(0, 3); // yang ngaruh cuman size, karena cuman mau liat jumlah page
        Page<SeminarModel> pageSeminar= seminarService.getAllSeminarIsRecordedSearchBy(isRecorded, searchBy, pageable);
        int totalPages = pageSeminar.getTotalPages() != 0?pageSeminar.getTotalPages():1;

        model.addAttribute("totalPages", totalPages);

        return "fragments/footer";
    }

    @GetMapping("/rate/{id}")
    public String rateWebinarFormPage(@PathVariable("id") String id, Model model, Principal principal){
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());
        EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);

        if (enrollment == null) {
            model.addAttribute("message", "Belum Terdaftar");
            return "error/error-custom-message";
        }

        Integer rate = enrollment.getRate() != null ? enrollment.getRate() : -1;
        model.addAttribute("rate", rate);
        model.addAttribute("seminar", seminar);
        return "seminar/detail-webinar-beri-rating";
    }
    @PostMapping("/rate/{id}")
    public String rateWebinarSubmitPage(@PathVariable("id") String id,  @RequestParam("rate") Integer rate, Model model, Principal principal){
        SeminarModel seminar = seminarService.getSeminarBySeminarId(id);
        UserModel user = userService.getUserByEmail(principal.getName());
        EnrollmentModel enrollment = enrollmentService.getEnrollmentByUserAndSeminar(user, seminar);

        if (enrollment == null) {
            model.addAttribute("message", "Belum Terdaftar");
            return "error/error-custom-message";
        }

        enrollment.setRate(rate);
        enrollmentService.updateEnrollment(enrollment);
        model.addAttribute("seminar", seminar);
        model.addAttribute("rate", rate);
        return "seminar/detail-webinar-selesai-rating";
    }

    @GetMapping("/{id}/body-list-invite-dokter.html")
    public String getUninvitedDokterListBody(Model model, @PathVariable String id, @RequestParam(required = false) String searchBy) {
        searchBy = searchBy==null?"":searchBy;
        List<DokterModel> listUnivitedDokter = dokterService.getUninvitedDokterSearchBy(id, searchBy);
        model.addAttribute("listUninvitedDokter", listUnivitedDokter);
        model.addAttribute("seminarId", id);
        model.addAttribute("inviteDokterResponse", new InviteDokterResponse());
        return "fragments/body-list-invite-dokter";
    }

    @PostMapping("/{id}/invite-dokter")
    public ModelAndView postInvitedDokter(Model model, @PathVariable String id, @ModelAttribute InviteDokterResponse inviteDokterResponse){
        SeminarModel gettedSeminar = new SeminarModel();
        gettedSeminar.setSeminarId(id);
        for(DokterModel dokter : inviteDokterResponse.getListDokter()){
            EnrollmentModel newEnrollment = new EnrollmentModel();
            newEnrollment.setSeminar(gettedSeminar);
            newEnrollment.setUser(dokter);
            newEnrollment.setTipeEnrollment("pembicara");
            enrollmentService.createEnrollment(newEnrollment);
        }
        return new ModelAndView("redirect:/seminars/{id}");
    }

    @GetMapping("/upload-sertifikat/{seminarId}")
    public String getFormUploadSertifikat(Model model, @PathVariable String seminarId){
        model.addAttribute("seminarId", seminarId);
        return "sertifikat/kustomisasi-sertifikat";
    }

    @PostMapping("/upload-sertifikat/{seminarId}/preview")
    public String postPreviewSertifikat(Model model,
                                        @PathVariable String seminarId,
                                        @RequestParam(required = false) String colorHex,
                                        @RequestParam(required = false) String font,
                                        @RequestParam MultipartFile imageMultipart) {
        try {
            String base64ImagePreview = sertifikatService.createPreviewSertifikatBase64(imageMultipart, "Poppins","#000000" , true, 30);

            String base64ImageSubmit = Base64.getEncoder().encodeToString(imageMultipart.getBytes());

            model.addAttribute("originalFileName", imageMultipart.getOriginalFilename());
            model.addAttribute("base64ImageSubmit", base64ImageSubmit);
            model.addAttribute("base64ImagePreview", base64ImagePreview);
            model.addAttribute("colorHex", colorHex);
            model.addAttribute("font", font);
            model.addAttribute("seminarId", seminarId);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return "sertifikat/kustomisasi-sertifikat-preview";
    }

    @PostMapping("/upload-sertifikat/{seminarId}/submit")
    public ModelAndView postTemplateSertifikat(Model model,
                                               @PathVariable String seminarId,
                                               @RequestParam String originalFileName,
                                               @RequestParam(required = false) String colorHex,
                                               @RequestParam(required = false) String font,
                                               @RequestParam String base64ImageSubmit) {
        System.out.println(base64ImageSubmit);
        try {
            SeminarModel seminar = seminarService.findBySeminarId(seminarId);

            byte[] decode = Base64.getDecoder().decode(base64ImageSubmit);
            byte[] imageCompressed = sertifikatService.resizeImage(decode);

            String templateSertifikatUrl = storageService.uploadCertificateToCloudStorage(imageCompressed, seminar);

            System.out.println(templateSertifikatUrl);
            seminar.setCertificateUrl(templateSertifikatUrl);
            seminarService.createSeminar(seminar);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return new ModelAndView("redirect:/seminars/"+seminarId);
    }

}
