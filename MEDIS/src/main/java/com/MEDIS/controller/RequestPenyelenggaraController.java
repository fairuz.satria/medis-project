package com.MEDIS.controller;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.payload.EmailDetailsPayload;
import com.MEDIS.service.PenyelenggaraService;
import com.MEDIS.service.email.EmailService;
import com.MEDIS.service.request.RequestPenyelenggaraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RequestPenyelenggaraController {
    @Autowired
    RequestPenyelenggaraService requestService;

    @Autowired
    PenyelenggaraService penyelenggaraService;

    @Autowired
    EmailService emailService;

    // @GetMapping("/request-penyelenggara")
    // public String requestForm(Model model){
    //     RequestPenyelenggaraModel request = new RequestPenyelenggaraModel();
    //     model.addAttribute("request", request);
    //     return "form-request-penyelenggara";
    // }



    @GetMapping("/admin/dashboard/list-request-penyelenggara")
    public String listRequestPenyelenggaraLayout(Model model){
//        List<RequestPenyelenggaraModel> listRequest = requestService.getAllRequestPenyelenggara();
//        model.addAttribute("listRequest", listRequest);
        return "read-list-request-penyelenggara";
    }

    @GetMapping("/admin/dashboard/list-request-penyelenggara/table-body-request-penyelenggara.html")
    public String listRequestPenyelenggaraTableBody(Model model, @RequestParam(required = true) String requestStatus) {
        System.out.println(requestStatus);
        List<RequestPenyelenggaraModel> listRequest = null;
        if (requestStatus.equals("All")){
            listRequest = requestService.getAllRequestPenyelenggara();
        }else{
            listRequest = requestService.getAllRequestPenyelenggaraByStatus(requestStatus);
        }
        model.addAttribute("listRequest", listRequest);
        return "fragments/table-body-request-penyelenggara";
    }

    @GetMapping("/admin/dashboard/list-request-penyelenggara/detail/{requestId}")
    public String detailRequestPenyelenggara(Model model, @PathVariable String requestId){
        RequestPenyelenggaraModel request = requestService.getById(requestId);
        if(request != null){
            model.addAttribute("request", request);
            return "detail-request-penyelenggara";
        }
        return "fail";
    }


    @PostMapping(value = "/admin/dashboard/list-request-penyelenggara/detail/{requestId}")
    public String rejectRequestPenyelenggara(Model model, @PathVariable String requestId){
        RequestPenyelenggaraModel request = requestService.getById(requestId);
        if(request != null){
            request.setStatus("Declined");
            request = requestService.saveRequest(request);
            model.addAttribute("request", request);
            return "detail-request-penyelenggara";
        }
        return "fail";
    }

    @GetMapping(value = "/admin/dashboard/list-request-penyelenggara/detail/{requestId}/buat-akun")
    public String buatAkunRequestPenyelenggara(Model model, @PathVariable String requestId){
        RequestPenyelenggaraModel request = requestService.getById(requestId);
        if(request != null){

//            request.setStatus("Approved");
//            request = requestService.saveRequest(request);
            model.addAttribute("request", request);
            return "form-create-akun-penyelenggara";
        }
        return "fail";
    }

    @PostMapping(value = "/admin/dashboard/list-request-penyelenggara/detail/{requestId}/buat-akun")
    public ModelAndView buatAkunRequestPenyelenggaraPost(@ModelAttribute PenyelenggaraModel penyelenggara, Model model, @PathVariable String requestId){
        RequestPenyelenggaraModel request = requestService.getById(requestId);
        if(request != null){
            String tempPassword = penyelenggaraService.savePenyelenggara(penyelenggara, request);
            EmailDetailsPayload emailPayload = new EmailDetailsPayload();
            emailPayload.setSubject("MEDIS webinar organizer invitation");
            emailPayload.setRecipient(request.getEmail());
            emailPayload.setMsgBody("Halo!, we are glad to inform you, that you're request to cooperate with MEDIS have been accepted. We invite you to join our platform" +
                    " in http://www.medis-id.com/ and login with the account we have provided below" +
                    "\n" +
                    "email: " + request.getEmail() + "\n" +
                    "password: " + tempPassword +"\n" +
                    "Please login to our platform and immediately set your new password");

            emailService.sendSimpleMail(emailPayload);
//            request.setStatus("Approved");
//            request = requestService.saveRequest(request);
            return new ModelAndView("redirect:/admin/dashboard/list-request-penyelenggara/detail/" + request.getRequestId());
        }
        return new ModelAndView("fail");
    }




}
