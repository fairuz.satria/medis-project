package com.MEDIS.controller;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;
import com.MEDIS.security.UserDetailsImpl;
import com.MEDIS.service.EnrollmentService;
import com.MEDIS.service.PaymentService;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.TransaksiService;
import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Controller
@RequestMapping("/payments")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    SeminarService seminarService;

    @Autowired
    TransaksiService transaksiService;

    @PostMapping(value="/handler", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> notificationHandler(@RequestBody Map<String, Object> payload) {
        String notifResponse = null;
        System.out.println(payload.toString());

        if (!(payload.isEmpty()) && paymentService.verifySignatureKey((String) payload.get("signature_key"))) {
            String orderId = (String) payload.get("order_id");
            String transactionStatus = (String) payload.get("transaction_status");
            String transactionTime = (String) payload.get("transaction_time");
            String paymentType = (String) payload.get("payment_type");
            String fraudStatus = (String) payload.get("fraud_status");

            notifResponse = "Transaction notification received. Order ID: " + orderId + ". Transaction status: " + transactionStatus + ". Fraud status: " + fraudStatus;

            TransaksiModel transaksi = transaksiService.getTransaksiByOrderId(orderId);
            SeminarModel seminar = transaksi.getSeminar();
            UserModel user = transaksi.getUser();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            if (fraudStatus.equals("challenge")) {
                transaksi.setStatusTransaksi("challenge");
                transaksiService.updateTransaksi(transaksi);
                return new ResponseEntity<>(notifResponse, HttpStatus.OK);
            }

            if (transactionStatus.equals("settlement") || transactionStatus.equals("capture")) {
                // buat enrollment
                EnrollmentModel enrollment = new EnrollmentModel();
                enrollment.setId(new EnrollmentId(user.getUuid(), seminar.getSeminarId()));
                enrollment.setSeminar(seminar);
                enrollment.setUser(user);
                enrollment.setTransaksi(transaksi);
                transaksi.setEnrollment(enrollment);
                String settlementTime = (String) payload.get("settlement_time");
                transaksi.setSettlementTime(LocalDateTime.parse(settlementTime, formatter));
                transaksi.setStatusTransaksi("paid");
                enrollmentService.createEnrollment(enrollment);
            } else if (transactionStatus.equals("pending")) {
                seminar.setTiketTerjual(seminar.getTiketTerjual()+1);
                transaksi.setTransactionTime(LocalDateTime.parse(transactionTime, formatter));
                transaksi.setPaymentType(paymentType);
                transaksi.setStatusTransaksi("waiting for payment");
                seminarService.updateSeminar(seminar);
            } else if (transactionStatus.equals("expire") || transactionStatus.equals("deny") || transactionStatus.equals("cancel")) {
                seminar.setTiketTerjual(seminar.getTiketTerjual()-1);
                transaksi.setStatusTransaksi("failed");
                seminarService.updateSeminar(seminar);
            }
            // set status transaksi
            transaksi.setStatusCause(transactionStatus);
            transaksiService.updateTransaksi(transaksi);
        }

        return new ResponseEntity<>(notifResponse, HttpStatus.OK);
    }

    // kalau berhasil bayar
    // -> isPaid = true
    // -> ubah status

    // kalau gagal
    // -> snapToken jadi null
    // -> ubah status

    // atau buat enrollment baru (?)
    // kayaknya perlu enrollment baru karena butuh order-id baru
}
