package com.MEDIS.controller;

import com.MEDIS.model.request.RequestInvitedSpeaker;
import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.payload.EmailDetailsPayload;
import com.MEDIS.service.InvitedSpeakerService;
import com.MEDIS.service.SeminarService;
import com.MEDIS.service.email.EmailService;
import com.MEDIS.service.request.RequestInvitedSpeakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
public class RequestInvitedSpeakerController {

    @Autowired
    SeminarService seminarService;

    @Autowired
    RequestInvitedSpeakerService requestService;

    @Autowired
    InvitedSpeakerService invitedSpeakerService;

    @Autowired
    EmailService emailService;

    @PostMapping(value="seminars/{seminarId}/request-invited-speaker", params={"requestInvitedSpeaker"})
    public ModelAndView detailSeminarPenyelenggaraPost(@ModelAttribute RequestInvitedSpeaker request, @PathVariable String seminarId, Model model){
        System.out.println(request.getSeminar());
        requestService.saveRequest(request);
        model.addAttribute("seminar",  seminarService.findBySeminarId(seminarId));
        return new ModelAndView("redirect:/seminars/"+seminarId);
    }

    @GetMapping("/admin/dashboard/list-request-invited-speaker")
    public String listRequestInvitedSpeaker(Model model){
//        List<RequestPenyelenggaraModel> listRequest = requestService.getAllRequestPenyelenggara();
//        model.addAttribute("listRequest", listRequest);
        return "read-list-request-inv-speaker";
    }

    @GetMapping("/admin/dashboard/list-request-invited-speaker/table-body-request-inv-speaker.html")
    public String listRequestInvitedSpeakerTableBody(Model model, @RequestParam(required = true) String requestStatus) {
        System.out.println(requestStatus);
        List<RequestInvitedSpeaker> listRequest = null;
        if (requestStatus.equals("All")){
            listRequest = requestService.getAllRequestInvitedSpeaker();
        }else{
            listRequest = requestService.getAllRequestInvitedSpeakerByStatus(requestStatus);
        }
        model.addAttribute("listRequest", listRequest);
        return "fragments/table-body-request-inv-speaker";
    }

    @GetMapping("/admin/dashboard/list-request-invited-speaker/detail/{requestId}")
    public String detailRequestPenyelenggara(Model model, @PathVariable String requestId){
        RequestInvitedSpeaker request = requestService.getById(requestId);
        if(request != null){
            model.addAttribute("request", request);
            return "detail-request-inv-speaker";
        }
        return "fail";
    }

    @PostMapping(value = "/admin/dashboard/list-request-invited-speaker/detail/{requestId}")
    public String rejectRequestInvitedSpeaker(Model model, @PathVariable String requestId){
        RequestInvitedSpeaker request = requestService.getById(requestId);
        if(request != null){
            request.setStatus("Declined");
            request = requestService.saveRequest(request);
            model.addAttribute("request", request);
            return "detail-request-inv-speaker";
        }
        return "fail";
    }

    @GetMapping(value = "/admin/dashboard/list-request-invited-speaker/detail/{requestId}/buat-akun")
    public String buatAkunRequestInvitedSpeaker(Model model, @PathVariable String requestId){
        RequestInvitedSpeaker request = requestService.getById(requestId);
        if(request != null){

//            request.setStatus("Approved");
//            request = requestService.saveRequest(request);
            model.addAttribute("request", request);
            return "form-create-akun-inv-speaker";
        }
        return "fail";
    }

    @PostMapping(value = "/admin/dashboard/list-request-invited-speaker/detail/{requestId}/buat-akun")
    public ModelAndView buatAkunRequestPenyelenggaraPost(@PathVariable String requestId,
                                                         @RequestParam("masaBerlaku") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime masaBerlaku,
                                                         Model model){
        RequestInvitedSpeaker request = requestService.getById(requestId);
        if(request != null){
            String tempPassword = invitedSpeakerService.addInvitedSpeaker(request, masaBerlaku);
            EmailDetailsPayload emailPayload = new EmailDetailsPayload();
            emailPayload.setSubject("MEDIS speaker invitation");
            emailPayload.setRecipient(request.getEmail());
            emailPayload.setMsgBody("Halo!, we want to inform you, that you've been invited to MEDIS platform to be an invited speaker on " + "'" + request.getSeminar().getTitle() + "' webinar" +
                    "\n" +
                    "And here is your account.\n" +
                    "email: " + request.getEmail() + "\n" +
                    "password: " + tempPassword +"\n" +
                    "Please login to our platform and immediately set your new password");

            emailService.sendSimpleMail(emailPayload);
//            request.setStatus("Approved");
//            request = requestService.saveRequest(request);
            return new ModelAndView("redirect:/admin/dashboard/list-request-invited-speaker/detail/" + requestId);
        }
        return new ModelAndView("fail");
    }
}
