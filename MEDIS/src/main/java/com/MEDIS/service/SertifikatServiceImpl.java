package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.SertifikatModel;
import com.MEDIS.repository.EnrollmentDb;
import com.MEDIS.repository.SertifikatDb;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

@Service
public class SertifikatServiceImpl implements SertifikatService{

    @Autowired
    StorageService storageService;

    @Autowired
    SertifikatDb sertifikatDb;

    @Autowired
    EnrollmentDb enrollmentDb;

    @Override
    public String createPreviewSertifikatBase64(MultipartFile image, String fontString, String colorHexString, Boolean bold, Integer fontSize) {
        try{
            Font font = new Font("Poppins", Font.PLAIN, 36);
            Color color = Color.decode(colorHexString);
            BufferedImage sertifikat = createSertifikat("Fairuz Satria Mahardika", font, bold, fontSize, color, image, "","test");

            // Convert the overlayedImage to Base64
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(sertifikat, "png", baos);
            baos.flush();
            byte[] imageBytes = baos.toByteArray();
            baos.close();
            String base64Image = java.util.Base64.getEncoder().encodeToString(imageBytes);
            return base64Image;
        } catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }

    }

    @Override
    public SertifikatModel generateSertifikat(EnrollmentModel enrollment, SeminarModel seminarModel, UserModel dokterModel) throws IOException {

        Font font = new Font("Poppins", Font.PLAIN, 36);
        Color color = Color.decode("#000000");
        BufferedImage sertifikat = createSertifikat(dokterModel.getNama(), font, true, 36, color, null, seminarModel.getCertificateUrl() , "http://www.medis-id.com/sertifikat/"+dokterModel.getUuid()+"/"+seminarModel.getSeminarId());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(sertifikat, "png", baos);
        baos.flush();
        byte[] imageBytes = baos.toByteArray();

        String sertifUrl = storageService.uploadGenerateCertificateToCloudStorage(imageBytes, seminarModel);

        SertifikatModel newSertifikat = new SertifikatModel();
        newSertifikat.setSertifikatUrl(sertifUrl);
        newSertifikat.setEnrollment(enrollment);
        newSertifikat = sertifikatDb.save(newSertifikat);

        enrollment.setSertifikat(newSertifikat);
        enrollmentDb.save(enrollment);

        return newSertifikat;
    }

    private BufferedImage createSertifikat(String namaString, Font font, boolean bold, Integer fontSize, Color textColor, MultipartFile image, String imageUrl, String verifUrl) {


        try {
            // Load the uploaded image
            BufferedImage backgroundImage = null;
            if(image != null){
                backgroundImage = ImageIO.read(image.getInputStream());
            }else {
                backgroundImage = ImageIO.read(new URL(imageUrl));
            }

            backgroundImage = ByteToBufferedImage(resizeImage(BufferedImageToByte(backgroundImage)));

            // Set font and style
            int style = bold ? Font.BOLD : Font.PLAIN;
            int adjustedFontSize = fontSize;
            Font modifiedFont = font.deriveFont(style, adjustedFontSize);

            System.out.println(backgroundImage.getHeight());
            System.out.println(backgroundImage.getWidth());

            // Create a new image with the same dimensions as the background image
            BufferedImage overlayedImage = new BufferedImage(backgroundImage.getWidth(), backgroundImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

            Graphics2D graphics = overlayedImage.createGraphics();

            // Draw the background image
            graphics.drawImage(backgroundImage, 0, 0, null);

            // Set font and color for the text
            graphics.setFont(modifiedFont);
            graphics.setColor(textColor);
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            // Calculate the size of the text
            FontMetrics fontMetrics = graphics.getFontMetrics(modifiedFont);
            float textWidth = fontMetrics.stringWidth(namaString);
            float textHeight = fontMetrics.getHeight();

            // Calculate the position to center the text
            float x = (overlayedImage.getWidth() - textWidth) / 2;
            float y = (overlayedImage.getHeight() - textHeight) / 2 + fontMetrics.getAscent() - 40;
            System.out.println(x);
            System.out.println(y);

            // Draw the text on top of the image
            graphics.drawString(namaString, x, y);
            System.out.println("pass1");

            // Generate the barcode image
            BufferedImage barcodeImage = generateBarcodeImage(verifUrl);
            System.out.println("pass2");

            // Overlay the barcode
            int xBarcode = (int) (750); // X-coordinate of the overlay position
            int yBarcode = (int) (330); // Y-coordinate of the overlay position
            System.out.println(xBarcode);
            System.out.println(yBarcode);
            graphics.drawImage(barcodeImage, xBarcode, yBarcode, null);


            // Dispose the graphics object
            graphics.dispose();

            // return the image as PNG
            return overlayedImage;
        } catch (Exception e) {
            System.out.println("Error creating text image with overlay: " + e.getMessage());
            return null;
        }
    }

    private BufferedImage convertToRGB(BufferedImage image) {
        // Create a new RGB image with the same dimensions
        BufferedImage rgbImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

        // Draw the original image onto the RGB image
        Graphics2D graphics = rgbImage.createGraphics();
        graphics.drawImage(image, 0, 0, null);
        graphics.dispose();

        return rgbImage;
    }

    @Override
    public byte[] resizeImage(byte[] bytesImage) throws IOException {
        ByteArrayInputStream ios = new ByteArrayInputStream(bytesImage);
        BufferedImage originalImage = ImageIO.read(ios);

        //calculate heigh according to width
        float originalWidht = originalImage.getWidth();
        float originalHeight = originalImage.getHeight();
        float targetWidth = 958;
        float targetHeight = (targetWidth/originalWidht)*originalHeight;

        Image resultingImage = originalImage.getScaledInstance((int) targetWidth, (int) targetHeight, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage((int) targetWidth, (int) targetHeight, BufferedImage.TYPE_INT_RGB);
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(outputImage, "png", baos);
        bytesImage = baos.toByteArray();

        ios.close();
        baos.close();

        return bytesImage;
    }

    private BufferedImage compressImage(BufferedImage backgroundImage) {
        try {
            // Convert the input image to RGB color space
            BufferedImage rgbImage = convertToRGB(backgroundImage);

            // Create a ByteArrayOutputStream to hold the compressed image data
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            // Specify the desired quality as a percentage (e.g., 80%)
            float quality = 0.00000000001f;

            // Create an ImageWriter for JPEG format
            ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();

            // Get the default parameters for the writer
            ImageWriteParam param = writer.getDefaultWriteParam();

            // Set the compression quality
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);

            // Create a memory cache ImageOutputStream
            MemoryCacheImageOutputStream imageOutputStream = new MemoryCacheImageOutputStream(outputStream);

            // Set the output destination
            writer.setOutput(imageOutputStream);

            // Write the image with the specified parameters
            writer.write(null, new IIOImage(rgbImage, null, null), param);

            // Close the resources
            writer.dispose();

            // Convert the compressed image data back to a BufferedImage
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            BufferedImage compressedImage = ImageIO.read(inputStream);

            // Close the input stream
            inputStream.close();

            return compressedImage;
        } catch (Exception e){
            System.out.println("compress error: "+e.getMessage());
            return null;
        }
    }

    private static BufferedImage generateBarcodeImage(String text) {
        int widht = (int) (150);
        int height = (int) (150);
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, widht, height);

            BufferedImage image = new BufferedImage(widht, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < widht; x++) {
                for (int y = 0; y < height; y++) {
                    int grayValue = bitMatrix.get(x, y) ? 0 : 255;
                    int rgbValue = (grayValue << 16) | (grayValue << 8) | grayValue;
                    image.setRGB(x, y, rgbValue);
                }
            }
            return image;

        } catch (WriterException e) {
            System.out.println("Error generating QR code: " + e.getMessage());
        }
        return null;
    }

    private byte[] BufferedImageToByte(BufferedImage backgroundImage) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(backgroundImage, "png", baos);
        baos.flush();
        byte[] imageBytes = baos.toByteArray();
        return imageBytes;
    }

    private BufferedImage ByteToBufferedImage(byte[] imageBytes) throws IOException {
        ByteArrayInputStream ios = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(ios);
        return image;
    }


}
