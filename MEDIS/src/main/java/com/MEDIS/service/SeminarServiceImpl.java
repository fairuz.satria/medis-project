package com.MEDIS.service;

import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.repository.SeminarDb;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.repository.PenyelenggaraDb;
import com.MEDIS.repository.SeminarDb;
import com.MEDIS.repository.UserDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SeminarServiceImpl implements SeminarService{

    @Autowired
    SeminarDb seminarDb;

    @Autowired
    PenyelenggaraDb penyelenggaraDb;

    @Override
    public SeminarModel getSeminarBySeminarId(String seminarId){
        return seminarDb.findBySeminarId(seminarId).get();
    }

    @Override
    public SeminarModel createSeminar(SeminarModel seminar) {
        return seminarDb.save(seminar);
    }

    @Override
    public SeminarModel updateSeminar(SeminarModel seminar) {
        return seminarDb.save(seminar);
    }

    @Override
    public List<SeminarModel> getListSeminar() {
        return seminarDb.findAll();
    }


    @Override
    public SeminarModel findBySeminarId(String seminarId){
        return seminarDb.findById(seminarId).get();
    }

    @Override
    public List<SeminarModel> getAllSeminarIsRecorded(Boolean isRecorded){
        return seminarDb.findByIsRecorded(isRecorded);
    }

    @Override
    public List<SeminarModel> getAllSeminarIsRecordedSearchBy(Boolean isRecorded, String searchBy){
        return seminarDb.findByIsRecordedAndTitleContaining(isRecorded, searchBy);
    }

    @Override
    public List<SeminarModel> getAllSeminarIsRecordedPenyelenggara(Boolean isRecorded, String username){
        PenyelenggaraModel penyelenggara = penyelenggaraDb.findByEmail(username).get();
        List<SeminarModel> listSeminar = penyelenggara.getListSeminar();
        List<SeminarModel> seminarreturned = new ArrayList<>();

        for(SeminarModel seminar : listSeminar){
            if(seminar.getIsRecorded() == isRecorded){
                seminarreturned.add(seminar);
            }
        }

        return seminarreturned;


    }

    @Override
    public List<SeminarModel> getAllSeminarIsRecordedPenyelenggaraSearchBy(Boolean isRecorded,String username, String searchBy){
        PenyelenggaraModel penyelenggara = penyelenggaraDb.findByEmail(username).get();
        List<SeminarModel> listSeminar = penyelenggara.getListSeminar();
        List<SeminarModel> seminarreturned = new ArrayList<>();

        for(SeminarModel seminar : listSeminar){
            if(seminar.getIsRecorded() == isRecorded && seminar.getTitle().contains(searchBy)){
                seminarreturned.add(seminar);
            }
        }

        return seminarreturned;
    }

    @Override
    public List<SeminarModel> getAllSeminarExamples() {
        return seminarDb.findBySeminarIdContains("uuid-test");
    }

    @Override
    public Integer getSeminarRate(SeminarModel seminar) {
        Integer sumRate = 0;
        Integer rateCount = 0;
        List<EnrollmentModel> listEnrollment = seminar.getListEnrollment();
        for ( EnrollmentModel enrollment : listEnrollment) {
            if (enrollment.getRate() != null) {
                sumRate += enrollment.getRate();
                rateCount++;
            }
        }
        return sumRate / rateCount;
    }

    @Override
    public Page<SeminarModel> getAllSeminarIsRecordedSearchBy(Boolean isRecorded, String searchBy, Pageable pageable) {
        return seminarDb.findByIsRecordedAndTitleContaining(isRecorded, searchBy, pageable);
    }

    public List<SeminarModel> getHistoryWebinarPenyelenggaraByStatus(PenyelenggaraModel penyelenggara, String status) {

        List<SeminarModel> filteredListSeminar = new ArrayList<>();
        List<SeminarModel> unfiltered = seminarDb.findSeminarModelByPenyelenggara(penyelenggara);

        if(status.equals("")){
            return unfiltered;
        }else{
             for(SeminarModel seminar : unfiltered){
                 if(seminar.getStatus().equals(status)){
                     filteredListSeminar.add(seminar);
                 }
             }
             return filteredListSeminar;
        }

    }
}
