package com.MEDIS.service;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.PenyelenggaraModel;

public interface PenyelenggaraService {
    String savePenyelenggara(PenyelenggaraModel penyelenggara, RequestPenyelenggaraModel request);
    PenyelenggaraModel savePenyelenggaraWithoutReq(PenyelenggaraModel penyelenggara);
    PenyelenggaraModel findPenyelenggaraByEmail(String email);
}
