package com.MEDIS.service;

import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.request.RequestInvitedSpeaker;
import com.MEDIS.model.user.InvitedSpeakerModel;

import java.util.List;
import java.time.LocalDateTime;




public interface InvitedSpeakerService {
    List<EnrollmentModel> getInvitedWebinar(String email, String status);
    String addInvitedSpeaker(RequestInvitedSpeaker request, LocalDateTime masaBerlaku);

    InvitedSpeakerModel getByEmail(String email);
}
