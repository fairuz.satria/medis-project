package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;

import java.util.List;

public interface TransaksiService {
    TransaksiModel getActiveTransaksi(UserModel user, SeminarModel seminar);
    TransaksiModel createTransaksi(TransaksiModel transaksi);
    TransaksiModel updateTransaksi(TransaksiModel transaksi);
    TransaksiModel getTransaksiByOrderId(String orderId);

    List<TransaksiModel> getListTransaksiByUser(UserModel user);

    List<SeminarModel> getListSeminarFromListTransaksi(List<TransaksiModel> listTransaksi);

    TransaksiModel getTransaksiByTransaksiId(String transaksiId);

    List<TransaksiModel> getListTransaksiByUserAndStatusTransaksi(UserModel user, String status);
}
