package com.MEDIS.service;

import com.MEDIS.model.FaqModel;
import com.MEDIS.repository.FaqDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaqServiceImpl implements FaqService{
    @Autowired
    FaqDb faqDb;

    @Override
    public List<FaqModel> getAllFaq() {
        return faqDb.findAll();
    }

    @Override
    public FaqModel getByFaqId(String faqId) {
        return faqDb.findById(faqId).get();
    }

    @Override
    public void save(FaqModel faq) {
        faqDb.save(faq);
    }

    @Override
    public void deleteFaq(FaqModel faq) {
        faqDb.delete(faq);
    }

    @Override
    public List<FaqModel> getAllFaqSearchBy(String searchBy){
        return faqDb.findByQuestionContaining(searchBy);
    }
}
