package com.MEDIS.service;

import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.repository.DokterDb;
import com.MEDIS.repository.UserDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DokterServiceImpl implements DokterService{

    @Autowired
    DokterDb dokterDb;

    @Override
    public DokterModel addDokter(DokterModel dokter){
        dokter.setPassword(UserServiceImpl.encrypt(dokter.getPassword()));
        return dokterDb.save(dokter);
    }

    @Override
    public void updateDokterDataDiri(DokterModel dokter){
        dokterDb.updateDokterDataDiri(dokter.getUuid(), dokter.getEmail(), dokter.getNama(), dokter.getTanggalLahir(), dokter.getJenisKelamin(), dokter.getPhotoUrl());
    }

    public static String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    @Override
    public DokterModel getByEmail(String email) {
        return dokterDb.findByEmail(email);
    }

    @Override
    public void updateDokterDataPekerjaan(DokterModel dokter) {
        dokterDb.updateDokterDataPekerjaan(dokter.getUuid(), dokter.getSpesialisasi(), dokter.getInstitusi(), dokter.getKotaKabupaten());
    }

    @Override
    public List<DokterModel> getUninvitedDokterSearchBy(String seminarId, String searchBy) {
        List<DokterModel> listUninvitedDokter = new ArrayList<>();
        List<DokterModel> listAllDokter = dokterDb.findByNamaContaining(searchBy);
        Integer count = 1;
        for(DokterModel dokter : listAllDokter){
            if(count > 5){
                break;
            }
            if(!isEnrolledTo(dokter, seminarId)){
                listUninvitedDokter.add(dokter);
                count++;
            }
        }
        return listUninvitedDokter;
    }

    @Override
    public Boolean isEnrolledTo(DokterModel dokter, String seminarId) {
        List<EnrollmentModel> listEnrollment = dokter.getListEnrollment();
        for(EnrollmentModel enrollment : listEnrollment){
            if(enrollment.getSeminar().getSeminarId().equals(seminarId)){
                return true;
            }
        }
        return false;
    }
}
