package com.MEDIS.service;

import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.repository.InvitedSpeakerDb;
import com.MEDIS.repository.RequestInvitedSpeakerDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import com.MEDIS.model.request.RequestInvitedSpeaker;
import com.MEDIS.model.user.InvitedSpeakerModel;
import com.MEDIS.repository.EnrollmentDb;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class InvitedSpeakerServiceImpl implements InvitedSpeakerService{

    @Autowired
    InvitedSpeakerDb invitedSpeakerDb;

    @Autowired
    RequestInvitedSpeakerDb requestDb;
    
    @Autowired
    EnrollmentDb enrollmentDb;


    @Override
    public List<EnrollmentModel> getInvitedWebinar(String email, String status) {
        List<EnrollmentModel> unfiltered = invitedSpeakerDb.findByEmail(email).get().getListEnrollment();
        switch(status){
            case "Upcoming": {
                List<EnrollmentModel> filtered = new ArrayList<>();
                for(EnrollmentModel enroll : unfiltered){
                    if(enroll.getSeminar().getStatus().equals("Upcoming")){
                        filtered.add(enroll);
                    }
                }
                return  filtered;}

            case "Done": {
                List<EnrollmentModel> filtered = new ArrayList<>();
                for(EnrollmentModel enroll : unfiltered){
                    if(enroll.getSeminar().getStatus().equals("Done")){
                        filtered.add(enroll);
                    }
                }

                return filtered;}

            default: {
                return unfiltered;}
        }
    }


    @Override
    public String addInvitedSpeaker(RequestInvitedSpeaker request, LocalDateTime masaBerlaku) {
        InvitedSpeakerModel invitedSpeaker = new InvitedSpeakerModel();
        String tempPassword = UUID.randomUUID().toString();

        invitedSpeaker.setNama(request.getNama());
        invitedSpeaker.setEmail(request.getEmail());
        invitedSpeaker.setMasaBerlaku(masaBerlaku);
        invitedSpeaker.setPassword(DokterServiceImpl.encrypt(tempPassword));
        invitedSpeaker.setRole("INVITED SPEAKER");
        invitedSpeaker.setRequest(request);

        invitedSpeaker = invitedSpeakerDb.save(invitedSpeaker);

        List<EnrollmentModel> listEnrollment = new ArrayList<>();
        EnrollmentModel enrollment = new EnrollmentModel();

        enrollment.setSeminar(request.getSeminar());
        enrollment.setUser(invitedSpeaker);
        enrollment.setTipeEnrollment("invited speaker: " + request.getPeran());

        enrollment = enrollmentDb.save(enrollment);

        listEnrollment.add(enrollment);

        invitedSpeaker.setListEnrollment(listEnrollment);

        request.setUser(invitedSpeaker);
        request.setStatus("Confirmed");
        requestDb.save(request);

        return tempPassword;
    }

    @Override
    public InvitedSpeakerModel getByEmail(String email){
        Optional<InvitedSpeakerModel> optInvitedSpeaker = invitedSpeakerDb.findByEmail(email);
        return optInvitedSpeaker.isEmpty()?null:optInvitedSpeaker.get();
    }
}
