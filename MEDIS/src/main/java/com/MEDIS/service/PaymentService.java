package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.midtrans.httpclient.error.MidtransError;

public interface PaymentService {
    String getSnapToken(String orderId, String grossAmount, UserModel user, SeminarModel seminar) throws MidtransError;
    Boolean verifySignatureKey(String signatureKey);
    String getClientKey();
}
