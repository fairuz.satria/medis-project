package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface StorageService {
    void uploadToCloudStorage(MultipartFile file, String fileName) throws IOException;
    void uploadToCloudStorage(byte[] fileBytes, String fileName) throws IOException;
    void deleteFileInCloudStorage(String fileName);
    String uploadVideoToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException;
    String uploadPdfToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException;
    String uploadImageToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException;
    String uploadCertificateToCloudStorage(byte[] imageBytes, SeminarModel seminar) throws IOException;
    String uploadGenerateCertificateToCloudStorage(byte[] imageBytes, SeminarModel seminar) throws IOException;



    String uploadImageToCloudStorage(MultipartFile file, UserModel user) throws IOException;
    String uploadCertificateToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException;
}
