package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.midtrans.Midtrans;
import com.midtrans.httpclient.SnapApi;
import com.midtrans.httpclient.error.MidtransError;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Value("${midtrans.sandbox.client.key}")
    String CLIENT_KEY_MIDTRANS_SANDBOX;
    @Value("${midtrans.sandbox.server.key}")
    String SERVER_KEY_MIDTRANS_SANDBOX;

    @Value("${midtrans.prod.client.key}")
    String CLIENT_KEY_MIDTRANS_PROD;
    @Value("${midtrans.prod.server.key}")
    String SERVER_KEY_MIDTRANS_PROD;

    @Autowired
    private Environment env;

    public PaymentServiceImpl() {
    }

    @Override
    public String getSnapToken(String orderId, String grossAmount, UserModel user, SeminarModel seminar) throws MidtransError {
        String activeProfile = env.getProperty("spring.profiles.active");
        if (activeProfile == null) activeProfile = env.getProperty("spring.profiles.default");
        boolean isProduction = activeProfile.equals("prod");

        // Set serverKey to Midtrans global config
        if (isProduction) {
            Midtrans.serverKey = SERVER_KEY_MIDTRANS_PROD;
        } else {
            Midtrans.serverKey = SERVER_KEY_MIDTRANS_SANDBOX;
        }

        // Set value to true if you want Production Environment (accept real transaction).
        Midtrans.isProduction = isProduction;


        Map<String, String> transactionDetails = new HashMap<>();
        transactionDetails.put("order_id", orderId);
        transactionDetails.put("gross_amount", grossAmount);
        Map<String, String> customerDetails = new HashMap<>();
        String[] nameSplit = user.getNama().split(" ");
        customerDetails.put("first_name", nameSplit[0]);
        customerDetails.put("last_name", nameSplit[nameSplit.length-1]);
        customerDetails.put("email", user.getEmail());
//        Map<String, String> itemDetails = new HashMap<>();
//        itemDetails.put("id", seminar.getSeminarId());
//        itemDetails.put("price", seminar.getHarga());
//        itemDetails.put("name", seminar.getTitle());
//        itemDetails.put("quantity", "1");
        Map<String, String> callbacks = new HashMap<>(); // TODO: tambahin callbacks kalau perlu
        callbacks.put("finish", "https://example.com/callbacks");

        Map<String, Object> params = new HashMap<>();
        params.put("transaction_details", transactionDetails);
        params.put("customer_details", customerDetails);
//        params.put("item_details", itemDetails);

        return SnapApi.createTransactionToken(params);
    }

    @Override
    public Boolean verifySignatureKey(String signatureKey) {
        return true;
    }
    // TODO: generate signature key terus dibandingin dengan signature key yang didapat

    @Override
    public String getClientKey() {
        String activeProfile = env.getProperty("spring.profiles.active");
        if (activeProfile == null) activeProfile = env.getProperty("spring.profiles.default");
        boolean isProduction = activeProfile.equals("prod");

        if (isProduction) return CLIENT_KEY_MIDTRANS_PROD;
        return CLIENT_KEY_MIDTRANS_SANDBOX;
    }
}
