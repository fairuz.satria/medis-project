package com.MEDIS.service;

import com.MEDIS.model.user.DokterModel;

import java.util.List;

public interface DokterService {
    DokterModel addDokter(DokterModel dokter);

    void updateDokterDataDiri(DokterModel dokter);

    DokterModel getByEmail(String username);

    void updateDokterDataPekerjaan(DokterModel dokter);

    List<DokterModel> getUninvitedDokterSearchBy(String id, String searchBy);

    Boolean isEnrolledTo(DokterModel dokter, String seminarId);
}
