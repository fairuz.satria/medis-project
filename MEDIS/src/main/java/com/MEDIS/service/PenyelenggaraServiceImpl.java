package com.MEDIS.service;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.repository.PenyelenggaraDb;
import com.MEDIS.repository.RequestPenyelenggaraDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PenyelenggaraServiceImpl implements PenyelenggaraService{
    @Autowired
    PenyelenggaraDb penyelenggaraDb;

    @Autowired
    RequestPenyelenggaraDb requestDb;

    @Override
    public String savePenyelenggara(PenyelenggaraModel penyelenggara, RequestPenyelenggaraModel request){
        String tempPassword = UUID.randomUUID().toString();

        penyelenggara.setNama(request.getNama());
        penyelenggara.setEmail(request.getEmail());
        penyelenggara.setPekerjaan(request.getPekerjaan());
        penyelenggara.setOrganisasi(request.getOrganisasi());
        penyelenggara.setNomorTelefon(request.getNomorTelefon());

        penyelenggara.setRole("PENYELENGGARA");
        penyelenggara.setPassword(encrypt(tempPassword));

        penyelenggara = penyelenggaraDb.save(penyelenggara);
        request.setUser(penyelenggara);
        request.setStatus("Confirmed");

        requestDb.save(request);

        return tempPassword;
    }

    @Override
    public PenyelenggaraModel savePenyelenggaraWithoutReq(PenyelenggaraModel penyelenggara){
        return penyelenggaraDb.save(penyelenggara);
    }

    @Override
    public PenyelenggaraModel findPenyelenggaraByEmail(String email) {
        return penyelenggaraDb.findByEmail(email).get();
    }

    public String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

}
