package com.MEDIS.service;

import com.MEDIS.model.webinar.SeminarModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public interface ZoomService {
    String generateSignature(String meetingNumber, int role);
    String getAccessTokenByAccountCredential() throws IOException, InterruptedException, JSONException;
    String getAccessTokenByAuthorizationCode(String authorizationCode) throws IOException, InterruptedException, JSONException;

    JSONObject createZoomMeeting(SeminarModel seminar) throws JSONException, IOException, InterruptedException;

    JSONObject getZoomMeeting(String meetingId) throws JSONException, IOException, InterruptedException;

    JSONObject endZoomMeeting(String meetingId) throws JSONException, IOException, InterruptedException;

    String getSdkKey();
}
