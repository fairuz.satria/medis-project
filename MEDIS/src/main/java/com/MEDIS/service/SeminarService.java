package com.MEDIS.service;

import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SeminarService {
    SeminarModel getSeminarBySeminarId(String seminarId);
    SeminarModel createSeminar(SeminarModel seminar);

    SeminarModel updateSeminar(SeminarModel seminar);
    List<SeminarModel> getListSeminar();
    SeminarModel findBySeminarId(String seminarId);
    List<SeminarModel> getAllSeminarIsRecorded(Boolean isRecorded);
    List<SeminarModel> getAllSeminarIsRecordedSearchBy(Boolean isRecorded, String searchBy);
    List<SeminarModel> getAllSeminarIsRecordedPenyelenggara(Boolean isRecorded, String username);
    List<SeminarModel> getAllSeminarIsRecordedPenyelenggaraSearchBy(Boolean isRecorded,String username, String searchBy);

    List<SeminarModel> getAllSeminarExamples();

    Integer getSeminarRate(SeminarModel seminar);

    Page<SeminarModel> getAllSeminarIsRecordedSearchBy(Boolean isRecorded, String searchBy, Pageable pageable);
    List<SeminarModel> getHistoryWebinarPenyelenggaraByStatus(PenyelenggaraModel penyelenggara, String status);
}
