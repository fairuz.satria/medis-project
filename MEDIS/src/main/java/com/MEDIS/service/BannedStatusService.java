package com.MEDIS.service;

import com.MEDIS.model.BannedStatusModel;

public interface BannedStatusService {
    BannedStatusModel saveBannedStatusModel(BannedStatusModel bannedStatusModel);
}
