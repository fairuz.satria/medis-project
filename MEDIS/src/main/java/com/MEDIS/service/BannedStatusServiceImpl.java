package com.MEDIS.service;

import com.MEDIS.model.BannedStatusModel;
import com.MEDIS.repository.BannedStatusDb;
import com.MEDIS.repository.EnrollmentDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BannedStatusServiceImpl implements BannedStatusService{

    @Autowired
    BannedStatusDb bannedStatusDb;

    @Override
    public BannedStatusModel saveBannedStatusModel(BannedStatusModel bannedStatusModel) {
        return bannedStatusDb.save(bannedStatusModel);
    }
}
