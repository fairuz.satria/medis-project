package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.checkerframework.checker.units.qual.A;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

@Service
public class StorageServiceImpl implements StorageService {

    @Override
    public void uploadToCloudStorage(MultipartFile file, String fileName) throws IOException {
//        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
//        System.out.println(credentials);

        Storage storage = StorageOptions.getDefaultInstance().getService();
        BlobId blobId = BlobId.of("medis_bucket", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(file.getContentType()).build();

        try (WriteChannel writer = storage.writer(blobInfo)) {
            try (InputStream inputStream = file.getInputStream()) {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    writer.write(ByteBuffer.wrap(buffer, 0, bytesRead));
                }
            }
            writer.close();
        }
    }

    @Override
    public void uploadToCloudStorage(byte[] fileBytes, String fileName) throws IOException {
        // GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        // System.out.println(credentials);

        Storage storage = StorageOptions.getDefaultInstance().getService();
        BlobId blobId = BlobId.of("medis_bucket", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("application/octet-stream").build();

        try (WriteChannel writer = storage.writer(blobInfo)) {
            try (InputStream inputStream = new ByteArrayInputStream(fileBytes)) {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    writer.write(ByteBuffer.wrap(buffer, 0, bytesRead));
                }
            }
        }
    }

    @Override
    public void deleteFileInCloudStorage(String fileName) {
        System.out.println(fileName);
        Storage storage = StorageOptions.getDefaultInstance().getService();
        BlobId blobId = BlobId.of("medis_bucket", fileName);
        System.out.println(storage.get(blobId));
        if (storage.get(blobId) != null) {
            System.out.println("delete: "+storage.delete(blobId));
        }
    }
    //    @Async
    @Override
    public String uploadPdfToCloudStorage(MultipartFile file, SeminarModel seminar)  throws IOException{
        // file type validation
        // ...
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        String fileName = String.format("files/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (seminar.getFileUrl() != null) deleteFileInCloudStorage(seminar.getFileUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(file, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadCertificateToCloudStorage(byte[] imageBytes, SeminarModel seminar) throws IOException {
        // file type validation
        // ...
        String extension = "png";
        String fileName = String.format("certificate/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (seminar.getCertificateUrl() != null) deleteFileInCloudStorage(seminar.getCertificateUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(imageBytes, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadGenerateCertificateToCloudStorage(byte[] imageBytes, SeminarModel seminar) throws IOException {
        String extension = "png";
        String fileName = String.format("certificate/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

        uploadToCloudStorage(imageBytes, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadVideoToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException {
        // file type validation
        // ...
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        String fileName = String.format("videos/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (seminar.getVideoUrl() != null) deleteFileInCloudStorage(seminar.getVideoUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(file, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadImageToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException {
        // file type validation
        // ...
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        String fileName = String.format("images/seminars/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (seminar.getPhotoUrl() != null) deleteFileInCloudStorage(seminar.getPhotoUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(file, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadImageToCloudStorage(MultipartFile file, UserModel user) throws IOException {
        // file type validation
        // ...
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        String fileName = String.format("images/users/%s-%s.%s", user.getUuid(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (user.getPhotoUrl() != null) deleteFileInCloudStorage(user.getPhotoUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(file, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

    @Override
    public String uploadCertificateToCloudStorage(MultipartFile file, SeminarModel seminar) throws IOException {
        // file type validation
        // ...
        String extension = StringUtils.getFilenameExtension(file.getOriginalFilename());
        String fileName = String.format("certificate/%s-%s.%s", seminar.getSeminarId(), UUID.randomUUID(), extension);

//        hapus file kalau udh ada
        // TODO: kayaknya perlu dipisah aja
        if (seminar.getCertificateUrl() != null) deleteFileInCloudStorage(seminar.getPhotoUrl().replace("https://storage.googleapis.com/medis_bucket/", "").replace("%20", " "));

        uploadToCloudStorage(file, fileName);
        String fileUrl = String.format("https://storage.googleapis.com/%s/%s", "medis_bucket", fileName.replace(" ", "%20"));
        return fileUrl;
    }

}
