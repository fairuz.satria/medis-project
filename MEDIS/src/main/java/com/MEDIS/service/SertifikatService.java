package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.SertifikatModel;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import java.awt.image.BufferedImage;
import java.io.IOException;

public interface SertifikatService {
    String createPreviewSertifikatBase64(MultipartFile image, String fontString, String colorString, Boolean bold, Integer fontSize) throws IOException;

    SertifikatModel generateSertifikat(EnrollmentModel enrollment, SeminarModel seminarModel, UserModel userModel) throws IOException;
    byte[] resizeImage(byte[] bytesImage) throws IOException;
}
