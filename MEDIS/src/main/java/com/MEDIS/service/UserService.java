package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import org.apache.catalina.User;

import java.util.List;

import java.util.List;
import java.util.Set;

public interface UserService {
    UserModel getUserByUuid(String uuid);

    UserModel getUserByEmail(String email);

    UserModel getUserByForgotUrl(String forgotUrl);

    UserModel addUser(UserModel user);

    List<EnrollmentModel> getHistoryWebinarByStatus(String username, String status);
    List<UserModel> getUserBySearch(String search);

    List<UserModel> getUserByBannedStatusSearch(String status, String search);

    List<UserModel> getUserByBannedStatusRoleSearch(String status, String role, String search);

    List<UserModel> getUserByRoleSearch(String role, String search);

    UserModel saveUserNoEncrpyt(UserModel user);
}
