package com.MEDIS.service;

import com.MEDIS.model.FaqModel;

import java.util.List;

public interface FaqService {
    List<FaqModel> getAllFaq();

    FaqModel getByFaqId(String faqId);

    void save(FaqModel faq);

    void deleteFaq(FaqModel faq);

    List<FaqModel> getAllFaqSearchBy(String searchBy);
}
