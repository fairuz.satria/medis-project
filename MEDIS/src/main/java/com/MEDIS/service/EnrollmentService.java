package com.MEDIS.service;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;

public interface EnrollmentService {
    boolean isEnrolled(UserModel user, SeminarModel seminar);
    EnrollmentModel createEnrollment(EnrollmentModel enrollment);
    EnrollmentModel getEnrollmentByUserAndSeminar(UserModel user, SeminarModel seminar);

    EnrollmentModel updateEnrollment(EnrollmentModel enrollment);

    EnrollmentModel getEnrollmentById(EnrollmentId enrollmentId);
}
