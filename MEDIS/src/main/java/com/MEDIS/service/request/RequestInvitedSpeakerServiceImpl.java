package com.MEDIS.service.request;

import com.MEDIS.model.request.RequestInvitedSpeaker;
import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.repository.RequestInvitedSpeakerDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestInvitedSpeakerServiceImpl implements RequestInvitedSpeakerService{
    @Autowired
    RequestInvitedSpeakerDb requestDb;

    @Override
    public RequestInvitedSpeaker saveRequest(RequestInvitedSpeaker request){
        return requestDb.save(request);
    }

    @Override
    public RequestInvitedSpeaker getById(String requestId) {
        return requestDb.findById(requestId).get();
    }

    @Override
    public List<RequestInvitedSpeaker> getAllRequestInvitedSpeaker() {
        return requestDb.findAll();
    }

    @Override
    public List<RequestInvitedSpeaker> getAllRequestInvitedSpeakerByStatus(String requestStatus) {
        return requestDb.findByStatus(requestStatus);
    }
}
