package com.MEDIS.service.request;

import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.repository.RequestPenyelenggaraDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestPenyelenggaraServiceImpl implements RequestPenyelenggaraService{

    @Autowired
    RequestPenyelenggaraDb requestDb;

    @Override
    public RequestPenyelenggaraModel saveRequest(RequestPenyelenggaraModel request){
        return requestDb.save(request);
    }

    @Override
    public List<RequestPenyelenggaraModel> getAllRequestPenyelenggara(){
        return requestDb.findAll();
    }

    @Override
    public RequestPenyelenggaraModel getById(String requestId){
        return requestDb.findByRequestId(requestId).get();
    }

    @Override
    public List<RequestPenyelenggaraModel> getAllRequestPenyelenggaraByStatus(String requestStatus){
        return requestDb.findByStatus(requestStatus);
    }
}
