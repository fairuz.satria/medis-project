package com.MEDIS.service.request;

import com.MEDIS.model.request.RequestPenyelenggaraModel;

import java.util.List;

public interface RequestPenyelenggaraService {
    RequestPenyelenggaraModel saveRequest(RequestPenyelenggaraModel request);
    List<RequestPenyelenggaraModel> getAllRequestPenyelenggara();
    List<RequestPenyelenggaraModel> getAllRequestPenyelenggaraByStatus(String requestStatus);
    RequestPenyelenggaraModel getById(String requestId);
}
