package com.MEDIS.service.request;

import com.MEDIS.model.request.RequestInvitedSpeaker;
import com.MEDIS.model.request.RequestPenyelenggaraModel;

import java.util.List;

public interface RequestInvitedSpeakerService {
    RequestInvitedSpeaker saveRequest(RequestInvitedSpeaker request);

    RequestInvitedSpeaker getById(String requestId);

    List<RequestInvitedSpeaker> getAllRequestInvitedSpeaker();

    List<RequestInvitedSpeaker> getAllRequestInvitedSpeakerByStatus(String requestStatus);
}
