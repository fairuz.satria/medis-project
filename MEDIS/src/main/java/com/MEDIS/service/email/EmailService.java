package com.MEDIS.service.email;

import com.MEDIS.payload.EmailDetailsPayload;
 
// Interface
public interface EmailService {
 
    // Method
    // To send a simple email
    String sendSimpleMail(EmailDetailsPayload details);
 
    // Method
    // To send an email with attachment
    // String sendMailWithAttachment(EmailDetails details);
}
