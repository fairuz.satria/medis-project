package com.MEDIS.service;

import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.repository.UserDb;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDb userDb;

    @Override
    public UserModel getUserByUuid(String uuid) {
        Optional<UserModel> user = userDb.findByUuid(uuid);
        return user.orElse(null);
    }

    @Override
    public UserModel getUserByEmail(String email){
        Optional<UserModel> user = userDb.findByEmail(email);
        if(!user.isEmpty()){
            return user.get();
        }
        return null;

    }

    @Override
    public UserModel getUserByForgotUrl(String forgotUrl) {
        return userDb.findByForgotUrl(forgotUrl).get();
    }

    @Override
    public List<UserModel> getUserBySearch(String search) {
        return userDb.findByEmailContainingIgnoreCase(search);
    }

    @Override
    public List<EnrollmentModel> getHistoryWebinarByStatus(String username, String status) {
        List<EnrollmentModel> unfiltered = getUserByEmail(username).getListEnrollment();
        switch(status){
            case "Upcoming": {
            List<EnrollmentModel> filtered = new ArrayList<>();
                for(EnrollmentModel enroll : unfiltered){
                    if(enroll.getSeminar().getStatus().equals("Upcoming")){
                        filtered.add(enroll);
                    }
                }
                return  filtered;}

            case "Done": {
                List<EnrollmentModel> filtered = new ArrayList<>();
                for(EnrollmentModel enroll : unfiltered){
                    if(enroll.getSeminar().getStatus().equals("Done")){
                        filtered.add(enroll);
                    }
                }

                return filtered;}

            default: {
                return unfiltered;}
        }

    }
    public List<UserModel> getUserByBannedStatusSearch(String status, String search) {
        return userDb.findByBannedStatusAndEmailContainingIgnoreCase(status, search);
    }

    @Override
    public List<UserModel> getUserByBannedStatusRoleSearch(String status, String role, String search) {
        return userDb.findByBannedStatusAndRoleAndEmailContainingIgnoreCase(status, role, search);
    }

    @Override
    public List<UserModel> getUserByRoleSearch(String role, String search) {
        return userDb.findByRoleAndEmailContainingIgnoreCase(role, search);
    }


    @Override
    public UserModel addUser(UserModel user) {
        user.setPassword(encrypt(user.getPassword()));
        return userDb.save(user);
    }

    @Override
    public UserModel saveUserNoEncrpyt(UserModel user) {
        return userDb.save(user);
    }

    public static String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    public static Boolean passwordMatches(String password, String matchingPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(matchingPassword, password);
    }

}
