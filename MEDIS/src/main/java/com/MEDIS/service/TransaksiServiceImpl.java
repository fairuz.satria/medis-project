package com.MEDIS.service;

import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.model.webinar.TransaksiModel;
import com.MEDIS.repository.TransaksiDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransaksiServiceImpl implements TransaksiService {

    @Autowired
    TransaksiDb transaksiDb;

    @Override
    public TransaksiModel getActiveTransaksi(UserModel user, SeminarModel seminar) {
        List<TransaksiModel> listTransaksi = transaksiDb.findByUserAndSeminar(user, seminar);
        for (TransaksiModel transaksi : listTransaksi) {
            // temporary berarti user belom milih paytment method
            // akan dibuat pas user pertama kali ke seminars/id
            // kalau belom ada model transaksi sama sekali
            if (transaksi.getStatusTransaksi().equals("temporary") || transaksi.getStatusTransaksi().equals("waiting for payment")) {
                return transaksi;
            }
        }
        return null;
    }

    @Override
    public TransaksiModel createTransaksi(TransaksiModel transaksi) {
        return transaksiDb.save(transaksi);
    }

    @Override
    public TransaksiModel updateTransaksi(TransaksiModel transaksi) {
        return transaksiDb.save(transaksi);
    }

    @Override
    public TransaksiModel getTransaksiByOrderId(String orderId) {
        return transaksiDb.findByOrderId(orderId);
    }

    @Override
    public List<TransaksiModel> getListTransaksiByUser(UserModel user) {
        List<TransaksiModel> listTransaksi = transaksiDb.findAllByUser(user);
        listTransaksi.removeIf(transaksi -> transaksi.getStatusTransaksi().equals("temporary"));
        return listTransaksi;
    }

    @Override
    public List<TransaksiModel> getListTransaksiByUserAndStatusTransaksi(UserModel user, String status) {
        List<TransaksiModel> listTransaksi = transaksiDb.findAllByUserAndStatusTransaksi(user, status);
        listTransaksi.removeIf(transaksi -> transaksi.getStatusTransaksi().equals("temporary"));
        return listTransaksi;
    }

    @Override
    public List<SeminarModel> getListSeminarFromListTransaksi(List<TransaksiModel> listTransaksi) {
        List<SeminarModel> listSeminar = new ArrayList<>();
        for (TransaksiModel transaksi:listTransaksi) {
            listSeminar.add(transaksi.getSeminar());
        }
        return listSeminar;
    }

    @Override
    public TransaksiModel getTransaksiByTransaksiId(String transaksiId) {
        return transaksiDb.findById(transaksiId).get();
    }
}
