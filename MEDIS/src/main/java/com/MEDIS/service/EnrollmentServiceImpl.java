package com.MEDIS.service;

import com.MEDIS.model.Id.EnrollmentId;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.repository.DokterDb;
import com.MEDIS.repository.EnrollmentDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {

    @Autowired
    EnrollmentDb enrollmentDb;

    @Override
    public boolean isEnrolled(UserModel user, SeminarModel seminar) {
        List<EnrollmentModel> listEnrollment = seminar.getListEnrollment();

        for (EnrollmentModel enrollment:listEnrollment) {
            if (enrollment.getUser().equals(user)) return true;
        }
        return false;
    }

    @Override
    public EnrollmentModel createEnrollment(EnrollmentModel enrollment) {
        return enrollmentDb.save(enrollment);
    }

    @Override
    public EnrollmentModel getEnrollmentByUserAndSeminar(UserModel user, SeminarModel seminar) {
        return enrollmentDb.findByUserAndSeminar(user, seminar);
    }

    @Override
    public EnrollmentModel updateEnrollment(EnrollmentModel enrollment) {
        return enrollmentDb.save(enrollment);
    }


    public EnrollmentModel getEnrollmentById(EnrollmentId enrollmentId) {
        return enrollmentDb.findById(enrollmentId).get();
    }
}
