package com.MEDIS.service;

import com.MEDIS.model.webinar.SeminarModel;
import io.jsonwebtoken.security.Keys;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Service
public class ZoomServiceImpl implements ZoomService {
    @Value("${zoom.sdk.client.id}")
    String CLIENT_ID_FOR_SDK;
    @Value("${zoom.sdk.client.secret}")
    String CLIENT_SECRET_FOR_SDK;

    @Value("${zoom.oauth.client.id}")
    String CLIENT_ID_FOR_OAUTH;
    @Value("${zoom.oauth.client.secret}")
    String CLIENT_SECRET_FOR_OAUTH;
    @Value("${zoom.oauth.account.id}")
    String ACCOUNT_ID_FOR_OAUTH;

    @Override
    public String generateSignature(String meetingNumber, int role) {
        try {
            String sdkKey = CLIENT_ID_FOR_SDK;
            String sdkSecret = CLIENT_SECRET_FOR_SDK;

            // Calculate iat and exp fields
            long iat = System.currentTimeMillis() / 1000 - 30;
            long exp = iat + 60 * 60 * 12; // 12 hours

//            System.out.println(iat);

            // Create header
            Map<String, Object> header = new HashMap<>();
            header.put("alg", "HS256");
            header.put("typ", "JWT");

            // Create payload
            Map<String, Object> payload = new HashMap<>();
            payload.put("sdkKey", sdkKey);
            payload.put("iat", iat);
            payload.put("exp", exp);
            payload.put("mn", meetingNumber);
            payload.put("role", role);

            // Serialize header and payload to JSON strings
            String encodedHeader = Base64.getUrlEncoder().encodeToString(new JSONObject(header).toString().getBytes(StandardCharsets.UTF_8));
            String encodedPayload = Base64.getUrlEncoder().encodeToString(new JSONObject(payload).toString().getBytes(StandardCharsets.UTF_8));

            // Concatenate header and payload with a period
            String encodedData = encodedHeader + "." + encodedPayload;

            // Sign data using HMAC-SHA256 and sdkSecret
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(sdkSecret.getBytes(), "HmacSHA256");
            mac.init(secretKeySpec);
            byte[] signatureBytes = mac.doFinal(encodedData.getBytes());

            // Encode signature as base64
            String signature = Base64.getUrlEncoder().encodeToString(signatureBytes);

            // Concatenate encoded data and signature with a period to form final JWT
//            System.out.println(encodedData + "." + signature);
            return encodedData + "." + signature;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public String getAccessTokenByAccountCredential() throws IOException, InterruptedException, JSONException {
        String clientId = CLIENT_ID_FOR_OAUTH;
        String clientSecret = CLIENT_SECRET_FOR_OAUTH;
        String accountId = ACCOUNT_ID_FOR_OAUTH;


        String authString = clientId + ":" + clientSecret;
        String base64AuthString = Base64.getEncoder().encodeToString(authString.getBytes());

        // construct the request with the authorization header
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://zoom.us/oauth/token?grant_type=account_credentials&account_id="+accountId))
                .header("Authorization", "Basic " + base64AuthString)
                .POST(HttpRequest.BodyPublishers.noBody())
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

//        System.out.println(response.body());
        String responseBody = response.body();
        JSONObject responseJson = new JSONObject(responseBody);
        String accessToken = responseJson.getString("access_token");
//        System.out.println(accessToken);

        return accessToken;
    }

    @Override
    public String getAccessTokenByAuthorizationCode(String authorizationCode) throws IOException, InterruptedException, JSONException {
        return null;
    }


    public JSONObject createZoomMeeting(SeminarModel seminar) throws JSONException, IOException, InterruptedException {
        JSONObject requestBody = new JSONObject();
        requestBody.put("topic", seminar.getTitle());
        requestBody.put("type", 2);
        requestBody.put("start_time", seminar.getDateTime().toString());
        requestBody.put("duration", seminar.getDuration()); // in minutes
        requestBody.put("timezone", "Asia/Jakarta");
        requestBody.put("agenda", seminar.getDeskripsi());
        JSONObject settings = new JSONObject();
        settings.put("auto_recording", isRecorded(seminar.getAutoRecord())); //TODO: kalau mau auto record jadiin seminar.getAutoRecord()
        settings.put("mute_upon_entry", true);
        requestBody.put("settings", settings);
        String requestBodyString = requestBody.toString();

        String accessToken = getAccessTokenByAccountCredential();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.zoom.us/v2/users/me/meetings"))
                .header("Authorization", "Bearer " + accessToken)
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBodyString))
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        int statusCode = response.statusCode();
        String responseBody = response.body();
        JSONObject responseJson = new JSONObject(responseBody);

//        System.out.println("HTTP status code: " + statusCode);
//        System.out.println("Response body: " + responseBody);

        return responseJson;
    }

    @Override
    public JSONObject getZoomMeeting(String meetingId) throws JSONException, IOException, InterruptedException {
        String accessToken = getAccessTokenByAccountCredential();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.zoom.us/v2/meetings/" + meetingId))
                .header("Authorization", "Bearer " + accessToken)
                .header("Content-Type", "application/json")
                .GET()
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        int statusCode = response.statusCode();
        String responseBody = response.body();
        JSONObject responseJson = new JSONObject(responseBody);

        System.out.println("HTTP status code: " + statusCode);
        System.out.println("Response body: " + responseBody);

        return responseJson;
    }

    @Override
    public JSONObject endZoomMeeting(String meetingId) throws JSONException, IOException, InterruptedException {
        String accessToken = getAccessTokenByAccountCredential();

        JSONObject requestBody = new JSONObject();
        requestBody.put("action", "end");
        String requestBodyString = requestBody.toString();
        System.out.println(requestBodyString);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.zoom.us/v2/meetings/" + meetingId + "/status"))
                .header("Authorization", "Bearer " + accessToken)
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString(requestBodyString))
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        int statusCode = response.statusCode();
        String responseBody = response.body();

        System.out.println("HTTP status code: " + statusCode);
        System.out.println("Response body: " + responseBody);

        JSONObject responseJson = new JSONObject(responseBody);



        return responseJson;
    }

    @Override
    public String getSdkKey() {
        return CLIENT_ID_FOR_SDK;
    }

    public String isRecorded(boolean isRecorded) {
        if (isRecorded) {
//            System.out.println("cloud");
            return "cloud";}
        return "none";
    }




}
