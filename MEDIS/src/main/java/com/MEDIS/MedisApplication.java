package com.MEDIS;

import com.MEDIS.model.FaqModel;
import com.MEDIS.model.request.RequestPenyelenggaraModel;
import com.MEDIS.model.user.DokterModel;
import com.MEDIS.model.user.InvitedSpeakerModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.model.webinar.EnrollmentModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.repository.*;
import com.MEDIS.model.user.PenyelenggaraModel;
import com.MEDIS.model.webinar.SeminarModel;
import com.MEDIS.repository.RequestPenyelenggaraDb;
import com.MEDIS.repository.SeminarDb;
import com.MEDIS.service.PenyelenggaraService;
import com.MEDIS.service.UserServiceImpl;
import com.MEDIS.service.ZoomService;
import com.MEDIS.service.ZoomServiceImpl;
import com.google.api.client.util.DateTime;
import com.google.auth.oauth2.GoogleCredentials;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.FacesWebRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@SpringBootApplication
public class MedisApplication {
	private final UserDb userDb;
	private final FaqDb faqDb;

	public MedisApplication(UserDb userDb,
							FaqDb faqDb) {
		this.userDb = userDb;
		this.faqDb = faqDb;
	}

	public static void main(String[] args) {
		SpringApplication.run(MedisApplication.class, args);
	}
	@Bean
	CommandLineRunner run(SeminarDb seminarDb, RequestPenyelenggaraDb requestDb, PenyelenggaraDb penyelenggaraDb, DokterDb dokterDb, ZoomService zoomService, EnrollmentDb enrollmentDb, InvitedSpeakerDb invitedSpeakerDb) {
		return args -> {
			//encrypt
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

			SeminarModel seminar = new SeminarModel();
			seminar.setHarga("100");
			seminar.setJumlahTiket(102);
			seminar.setTiketTerjual(4);
			seminar.setLokasi("Jakarta");
			seminar.setNamaPerusahaan("fkui");
			seminar.setIsRecorded(false);
			seminar.setDeskripsi("Seminar terkait teknologi terbaru dalam diagnosis & pengobatan kanker. Mempelajari terapi gen & imunoterapi serta strategi penanganan kanker yang fokus pada pasien.");
			seminar.setTitle("Inovasi Terbaru dalam Diagnosis dan Pengobatan Kanker");
			seminar.setPhotoUrl("/img/image-one.jpeg");
			seminar.setDuration(45);
			seminar.setDateTime(LocalDateTime.now());

//			JSONObject seminarJson1 =  zoomService.createZoomMeeting(seminar);
			JSONObject seminarJson1 = new JSONObject();
			seminarJson1.put("uuid", "uuid-test-1");
			seminarJson1.put("id", "76725556443");
			seminarJson1.put("join_url", "https://us04web.zoom.us/j/76725556443?pwd=DMfIaveqc4SkDrbRRsmthfEroNxZ8f.1");
			seminarJson1.put("password", "LM6RYj");
			seminar.setSeminarId(seminarJson1.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar.setMeetingId(seminarJson1.getString("id"));
			seminar.setJoinUrl(seminarJson1.getString("join_url"));
			seminar.setPassword(seminarJson1.getString("password"));


			SeminarModel seminar2 = new SeminarModel();
			seminar2.setHarga("150");
			seminar2.setJumlahTiket(70);
			seminar2.setTiketTerjual(7);
			seminar2.setLokasi("Bekasi");
			seminar2.setNamaPerusahaan("fkui");
			seminar2.setIsRecorded(false);
			seminar2.setDeskripsi("Seminar tentang penanganan penyakit kronis: pengelolaan stres, manajemen obat, dan nutrisi. Peserta akan belajar mengelola penyakit dan memfasilitasi pasien meningkatkan kualitas hidup.");
			seminar2.setTitle("Meningkatkan Kualitas Hidup Pasien dengan Penyakit Kronis");
			seminar2.setPhotoUrl("/img/image-three.jpeg");
			seminar2.setDuration(45);
			seminar2.setDateTime(LocalDateTime.now().plusDays(1));

//			JSONObject seminarJson2 =  zoomService.createZoomMeeting(seminar2);
			JSONObject seminarJson2 = new JSONObject();
			seminarJson2.put("uuid", "uuid-test-2");
			seminarJson2.put("id", "77263584460");
			seminarJson2.put("join_url", "https://us04web.zoom.us/j/77263584460?pwd=TSzRK9GaanjGRbAlauowDPBpicspwJ.1");
			seminarJson2.put("password", "hGiTY9");
			seminar2.setSeminarId(seminarJson2.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar2.setMeetingId(seminarJson2.getString("id"));
			seminar2.setJoinUrl(seminarJson2.getString("join_url"));
			seminar2.setPassword(seminarJson2.getString("password"));


			SeminarModel seminar3 = new SeminarModel();
			seminar3.setHarga("700000");
			seminar3.setJumlahTiket(50);
			seminar3.setTiketTerjual(50);
			seminar3.setLokasi("Tangerang");
			seminar3.setNamaPerusahaan("fkui");
			seminar3.setIsRecorded(false);
			seminar3.setDeskripsi("Seminar yang membahas terkait kesadaran kesehatan mental di lingkungan kerja. Seminar ini mempelajari cara mengenali gejala masalah kesehatan mental & atasi stres di tempat kerja.");
			seminar3.setTitle("Meningkatkan Kesadaran tentang Kesehatan Mental di Lingkungan Kerja");
			seminar3.setPhotoUrl("/img/image-two.jpeg");
			seminar3.setDuration(45);
			seminar3.setDateTime(LocalDateTime.now().plusDays(2));

//			JSONObject seminarJson3 =  zoomService.createZoomMeeting(seminar3);
			JSONObject seminarJson3 = new JSONObject();
			seminarJson3.put("uuid", "uuid-test-3");
			seminarJson3.put("id", "77372508922");
			seminarJson3.put("join_url", "https://us04web.zoom.us/j/77372508922?pwd=kd5aeAOIXSmcdiDKGFBrk7VGg3qzkY.1");
			seminarJson3.put("password", "3e2gMA");
			seminar3.setSeminarId(seminarJson3.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar3.setMeetingId(seminarJson3.getString("id"));
			seminar3.setJoinUrl(seminarJson3.getString("join_url"));
			seminar3.setPassword(seminarJson3.getString("password"));


			SeminarModel seminar4 = new SeminarModel();
			seminar4.setHarga("100000");
			seminar4.setJumlahTiket(102);
			seminar4.setTiketTerjual(4);
			seminar4.setLokasi("Jakarta");
			seminar4.setNamaPerusahaan("fkui");
			seminar4.setIsRecorded(false);
			seminar4.setDeskripsi("Diskusi interaktif dan workshop untuk menggali strategi terbaik dalam menangani masalah kesehatan mental yang berkembang pesat.");
			seminar4.setTitle("Kongres Memahami dan Mengatasi Tantangan Psikologis di Era Modern");
			seminar4.setPhotoUrl("/img/seminar-image-4.jpg");
			seminar4.setDuration(45);
			seminar4.setDateTime(LocalDateTime.now().plusDays(2));

//			JSONObject seminarJson1 =  zoomService.createZoomMeeting(seminar);
			JSONObject seminarJson4 = new JSONObject();
			seminarJson4.put("uuid", "uuid-test2-4");
			seminarJson4.put("id", "76725556443");
			seminarJson4.put("join_url", "https://us04web.zoom.us/j/76725556443?pwd=DMfIaveqc4SkDrbRRsmthfEroNxZ8f.1");
			seminarJson4.put("password", "LM6RYj");
			seminar4.setSeminarId(seminarJson4.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar4.setMeetingId(seminarJson4.getString("id"));
			seminar4.setJoinUrl(seminarJson4.getString("join_url"));
			seminar4.setPassword(seminarJson4.getString("password"));


			SeminarModel seminar5 = new SeminarModel();
			seminar5.setHarga("500000");
			seminar5.setJumlahTiket(70);
			seminar5.setTiketTerjual(7);
			seminar5.setLokasi("Bekasi");
			seminar5.setNamaPerusahaan("fkui");
			seminar5.setIsRecorded(false);
			seminar5.setDeskripsi("Bergabunglah dengan para ahli medis dan industri untuk menjelajahi teknologi canggih yang mengubah perawatan kesehatan.");
			seminar5.setTitle("Simposium Mendorong Kemajuan Diagnostik dan Terapi Kedokteran");
			seminar5.setPhotoUrl("/img/seminar-image-5.jpg");
			seminar5.setDuration(45);
			seminar5.setDateTime(LocalDateTime.now().minusDays(2));

//			JSONObject seminarJson2 =  zoomService.createZoomMeeting(seminar2);
			JSONObject seminarJson5 = new JSONObject();
			seminarJson5.put("uuid", "uuid-test2-5");
			seminarJson5.put("id", "77263584460");
			seminarJson5.put("join_url", "https://us04web.zoom.us/j/77263584460?pwd=TSzRK9GaanjGRbAlauowDPBpicspwJ.1");
			seminarJson5.put("password", "hGiTY9");
			seminar5.setSeminarId(seminarJson5.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar5.setMeetingId(seminarJson5.getString("id"));
			seminar5.setJoinUrl(seminarJson5.getString("join_url"));
			seminar5.setPassword(seminarJson5.getString("password"));


			SeminarModel seminar6 = new SeminarModel();
			seminar6.setHarga("700000");
			seminar6.setJumlahTiket(50);
			seminar6.setTiketTerjual(50);
			seminar6.setLokasi("Tangerang");
			seminar6.setNamaPerusahaan("fkui");
			seminar6.setIsRecorded(false);
			seminar6.setDeskripsi("Diskusikan penemuan terbaru dalam farmasi dan bagaimana memastikan manfaatnya dapat diakses oleh masyarakat secara luas.");
			seminar6.setTitle("Inovasi Farmasi Kesenjangan Antara Penelitian dan Pasien");
			seminar6.setPhotoUrl("/img/seminar-image-6.jpg");
			seminar6.setDuration(45);
			seminar6.setDateTime(LocalDateTime.now().plusDays(2));

//			JSONObject seminarJson3 =  zoomService.createZoomMeeting(seminar3);
			JSONObject seminarJson6 = new JSONObject();
			seminarJson6.put("uuid", "uuid-test2-6");
			seminarJson6.put("id", "77372508922");
			seminarJson6.put("join_url", "https://us04web.zoom.us/j/77372508922?pwd=kd5aeAOIXSmcdiDKGFBrk7VGg3qzkY.1");
			seminarJson6.put("password", "3e2gMA");
			seminar6.setSeminarId(seminarJson6.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar6.setMeetingId(seminarJson6.getString("id"));
			seminar6.setJoinUrl(seminarJson6.getString("join_url"));
			seminar6.setPassword(seminarJson6.getString("password"));


			SeminarModel seminar7 = new SeminarModel();
			seminar7.setHarga("100000");
			seminar7.setJumlahTiket(102);
			seminar7.setTiketTerjual(4);
			seminar7.setLokasi("Jakarta");
			seminar7.setNamaPerusahaan("fkui");
			seminar7.setIsRecorded(false);
			seminar7.setDeskripsi("Dapatkan tips dan strategi praktis untuk mencapai dan mempertahankan berat badan yang sehat melalui pendekatan yang holistik.");
			seminar7.setTitle("Simposium Strategi Pengendalian Berat Badan yang Efektif");
			seminar7.setPhotoUrl("/img/seminar-image-7.jpg");
			seminar7.setDuration(45);
			seminar7.setDateTime(LocalDateTime.now().plusDays(2));

//			JSONObject seminarJson1 =  zoomService.createZoomMeeting(seminar);
			JSONObject seminarJson7 = new JSONObject();
			seminarJson7.put("uuid", "uuid-test2-7");
			seminarJson7.put("id", "76725556443");
			seminarJson7.put("join_url", "https://us04web.zoom.us/j/76725556443?pwd=DMfIaveqc4SkDrbRRsmthfEroNxZ8f.1");
			seminarJson7.put("password", "LM6RYj");
			seminar7.setSeminarId(seminarJson7.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar7.setMeetingId(seminarJson7.getString("id"));
			seminar7.setJoinUrl(seminarJson7.getString("join_url"));
			seminar7.setPassword(seminarJson7.getString("password"));


			SeminarModel seminar8 = new SeminarModel();
			seminar8.setHarga("500000");
			seminar8.setJumlahTiket(70);
			seminar8.setTiketTerjual(7);
			seminar8.setLokasi("Bekasi");
			seminar8.setNamaPerusahaan("fkui");
			seminar8.setIsRecorded(false);
			seminar8.setDeskripsi("Pelajari cara menjaga kebugaran tubuh dan membangun gaya hidup aktif di lingkungan rumah. Temukan latihan sederhana yang dapat Anda lakukan tanpa peralatan khusus.");
			seminar8.setTitle("Seminar Fitness at Home: Building an Active Lifestyle for Health");
			seminar8.setPhotoUrl("/img/seminar-image-8.jpg");
			seminar8.setDuration(45);
			seminar8.setDateTime(LocalDateTime.now().minusDays(2));

//			JSONObject seminarJson2 =  zoomService.createZoomMeeting(seminar2);
			JSONObject seminarJson8 = new JSONObject();
			seminarJson8.put("uuid", "uuid-test2-8");
			seminarJson8.put("id", "77263584460");
			seminarJson8.put("join_url", "https://us04web.zoom.us/j/77263584460?pwd=TSzRK9GaanjGRbAlauowDPBpicspwJ.1");
			seminarJson8.put("password", "hGiTY9");
			seminar8.setSeminarId(seminarJson8.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar8.setMeetingId(seminarJson8.getString("id"));
			seminar8.setJoinUrl(seminarJson8.getString("join_url"));
			seminar8.setPassword(seminarJson8.getString("password"));


			SeminarModel seminar9 = new SeminarModel();
			seminar9.setHarga("700000");
			seminar9.setJumlahTiket(50);
			seminar9.setTiketTerjual(50);
			seminar9.setLokasi("Tangerang");
			seminar9.setNamaPerusahaan("fkui");
			seminar9.setIsRecorded(false);
			seminar9.setDeskripsi("Menjelajahi dampak penggunaan berlebihan perangkat digital dan pentingnya menjaga keseimbangan waktu layar. Pelajari cara mengurangi ketergantungan dan hidup secara lebih sadar.");
			seminar9.setTitle("Seminar Digital Detox:  Balancing Screen Time for Health");
			seminar9.setPhotoUrl("/img/seminar-image-9.jpg");
			seminar9.setDuration(45);
			seminar9.setDateTime(LocalDateTime.now().plusDays(2));

//			JSONObject seminarJson3 =  zoomService.createZoomMeeting(seminar3);
			JSONObject seminarJson9 = new JSONObject();
			seminarJson9.put("uuid", "uuid-test2-9");
			seminarJson9.put("id", "77372508922");
			seminarJson9.put("join_url", "https://us04web.zoom.us/j/77372508922?pwd=kd5aeAOIXSmcdiDKGFBrk7VGg3qzkY.1");
			seminarJson9.put("password", "3e2gMA");
			seminar9.setSeminarId(seminarJson9.getString("uuid").replace("/", "")); // mungkin perlu diganti
			seminar9.setMeetingId(seminarJson9.getString("id"));
			seminar9.setJoinUrl(seminarJson9.getString("join_url"));
			seminar9.setPassword(seminarJson9.getString("password"));






			//Penyelenggara & Seminar
			PenyelenggaraModel penyelengara = new PenyelenggaraModel();
			penyelengara.setPekerjaan("dokter");
			penyelengara.setOrganisasi("FKUI");
			penyelengara.setNama("penyelenggara");
			penyelengara.setEmail("penyelenggara@gmail.com");
			penyelengara.setNomorTelefon("085162626200");
			penyelengara.setKuotaPembuatan(9);
			penyelengara.setRole("PENYELENGGARA");
			penyelengara.setPassword(passwordEncoder.encode("medis"));

			UserModel admin = new UserModel();
			admin.setNama("admin");
			admin.setRole("ADMIN");
			admin.setEmail("administrator@gmail.com");
			admin.setPassword(UserServiceImpl.encrypt("medis"));

			userDb.save(admin);

			List<SeminarModel> listSeminar = new ArrayList<>();
			listSeminar.add(seminar);
			listSeminar.add(seminar3);
			listSeminar.add(seminar5);
			listSeminar.add(seminar7);
			listSeminar.add(seminar9);

			penyelengara = penyelenggaraDb.save(penyelengara);

			seminar.setPenyelenggara(penyelengara);
			seminar3.setPenyelenggara(penyelengara);
			seminar5.setPenyelenggara(penyelengara);
			seminar7.setPenyelenggara(penyelengara);
			seminar9.setPenyelenggara(penyelengara);

			PenyelenggaraModel penyelengara2= new PenyelenggaraModel();
			penyelengara2.setPekerjaan("dokter2");
			penyelengara2.setOrganisasi("FKUI2");
			penyelengara2.setNama("penyelenggara2");
			penyelengara2.setEmail("penyelenggara2@gmail.com");
			penyelengara2.setNomorTelefon("085162626200");
			penyelengara2.setKuotaPembuatan(10);
			penyelengara2.setRole("PENYELENGGARA");
			penyelengara2.setPassword(passwordEncoder.encode("medis"));

			List<SeminarModel> listSeminar2 = new ArrayList<>();
			listSeminar2.add(seminar2);
			listSeminar2.add(seminar4);
			listSeminar2.add(seminar6);
			listSeminar2.add(seminar8);

			penyelengara2.setListSeminar(listSeminar2);

			penyelengara2 = penyelenggaraDb.save(penyelengara2);

			seminar2.setPenyelenggara(penyelengara2);
			seminar4.setPenyelenggara(penyelengara2);
			seminar6.setPenyelenggara(penyelengara2);
			seminar8.setPenyelenggara(penyelengara2);

			seminar = seminarDb.save(seminar);
			seminarDb.save(seminar2);
			seminarDb.save(seminar3);
			seminarDb.save(seminar4);
			seminarDb.save(seminar5);
			seminarDb.save(seminar6);
			seminarDb.save(seminar7);
			seminarDb.save(seminar8);
			seminarDb.save(seminar9);

			;


			//Request Penyelenggara
			RequestPenyelenggaraModel request = new RequestPenyelenggaraModel();
			request.setNama("fairuz");
			request.setOrganisasi("fairuz inc");
			request.setEmail("fairuzsatriam@gmail.com");
			request.setPekerjaan("developer");
			request = requestDb.save(request);
			System.out.println(request.getStatus());

			request = new RequestPenyelenggaraModel();
			request.setNama("fairuz");
			request.setOrganisasi("fairuz inc");
			request.setEmail("fairuzsatriam@gmail.com");
			request.setPekerjaan("developer");
			request = requestDb.save(request);

			request = new RequestPenyelenggaraModel();
			request.setNama("fairuz");
			request.setOrganisasi("fairuz inc");
			request.setEmail("fairuzsatriam@gmail.com");
			request.setPekerjaan("developer");
			request.setStatus("Declined");
			request = requestDb.save(request);

			request = new RequestPenyelenggaraModel();
			request.setNama("fairuz");
			request.setOrganisasi("fairuz inc");
			request.setEmail("fairuzsatriam@gmail.com");
			request.setPekerjaan("developer");
			request.setStatus("Confirmed");
			request = requestDb.save(request);

			request = new RequestPenyelenggaraModel();
			request.setNama("fairuz");
			request.setOrganisasi("fairuz inc");
			request.setEmail("fairuzsatriam@gmail.com");
			request.setPekerjaan("developer");
			request = requestDb.save(request);

			DokterModel dokter = new DokterModel();
			dokter.setEmail("dokter@gmail.com");
			dokter.setNama("dokter");
			dokter.setInstitusi("Fakultas Kedokteran UI");
			dokter.setNomorTelefon("88888888");
			dokter.setTanggalLahir(LocalDate.now().minusYears(25));
			dokter.setPekerjaan("Dokter");
			dokter.setRole("DOKTER");
			dokter.setPassword(passwordEncoder.encode("medis"));
			dokter = dokterDb.save(dokter);

//			EnrollmentModel enrollmentModel = new EnrollmentModel();
//			enrollmentModel.setUser(dokter);
//			enrollmentModel.setSeminar(seminar);
//			enrollmentModel.setTipeEnrollment("pendaftar");
//			enrollmentDb.save(enrollmentModel);

			FaqModel faq1 = new FaqModel();
			faq1.setQuestion("Bagaimana cara mendaftar konferensi atau event di Medis?");
			faq1.setAnswer("Untuk mendaftar konferensi medis, pilih konferensi yang diinginkan dan cari informasi terkait tanggal, lokasi, biaya pendaftaran, dan persyaratan. Setelah itu, lakukan pendaftaran di situs web dan lengkapi formulir pendaftaran dengan data pribadi Anda. Setelah pendaftaran selesai, ikuti petunjuk pembayaran yang diberikan oleh penyelenggara konferensi, seperti transfer bank atau menggunakan platform pembayaran online. Pastikan membayar biaya pendaftaran sesuai dengan instruksi yang diberikan untuk mengkonfirmasi kehadiran Anda di konferensi medis tersebut.");

			FaqModel faq2 = new FaqModel();
			faq2.setQuestion("Bagaimana saya dapat mencari informasi spesifik di website medis ini?");
			faq2.setAnswer("Untuk mencari informasi spesifik di website medis ini, Anda dapat menggunakan fitur pencarian yang biasanya terletak di bagian atas atau samping halaman. Masukkan kata kunci terkait topik yang Anda cari, dan hasil yang relevan akan ditampilkan.");

			FaqModel faq3 = new FaqModel();
			faq3.setQuestion("Bagaimana proses pembayaran untuk konferensi medis ini?");
			faq3.setAnswer("Proses pembayaran dapat dilakukan melalui berbagai metode, termasuk transfer bank, kartu kredit, atau pembayaran melalui platform online. Informasi mengenai proses pembayaran akan tersedia di situs web resmi konferensi.");

			FaqModel faq4 = new FaqModel();
			faq4.setQuestion("Apakah sertifikat kehadiran akan diberikan kepada peserta konferensi medis ini?");
			faq4.setAnswer("Ya, sertifikat kehadiran akan diberikan kepada peserta konferensi medis. Setelah mengikuti konferensi, Anda akan menerima sertifikat kehadiran yang mencantumkan nama, tanggal, dan detail konferensi sebagai bukti partisipasi Anda.");

			FaqModel faq5 = new FaqModel();
			faq5.setQuestion("Apakah website medis ini menjaga kerahasiaan informasi pribadi pengguna?");
			faq5.setAnswer("Kami memprioritaskan dan menjaga kerahasiaan informasi pribadi pengguna dengan sungguh-sungguh. Kami mengikuti praktik keamanan dan privasi yang ketat untuk melindungi data pribadi yang diberikan kepada kami. Kami hanya menggunakan informasi pribadi sesuai dengan kebijakan privasi kami yang dapat diakses di situs web kami. Kami tidak membagikan informasi pribadi pengguna dengan pihak ketiga tanpa izin pengguna kecuali ada persetujuan atau kewajiban hukum yang mengharuskan hal tersebut. Kami mengambil langkah-langkah yang tepat untuk mencegah akses yang tidak sah atau penggunaan yang salah terhadap informasi pribadi pengguna.");

			faqDb.saveAll(Arrays.asList(faq1, faq2, faq3, faq4, faq5));

			//add base invited speaker
			InvitedSpeakerModel invitedSpeaker = new InvitedSpeakerModel();
			EnrollmentModel enrollmentInvitedSpeaker = new EnrollmentModel();

			invitedSpeaker.setEmail("invitedspeaker@gmail.com");
			invitedSpeaker.setNama("invitedspeaker");
			invitedSpeaker.setRole("INVITED SPEAKER");
			invitedSpeaker.setPassword(passwordEncoder.encode("medis"));
			invitedSpeaker.setMasaBerlaku(LocalDateTime.now().plusYears(1));
			invitedSpeaker.setListEnrollment(new ArrayList<>());

			invitedSpeaker = invitedSpeakerDb.save(invitedSpeaker);

			enrollmentInvitedSpeaker.setSeminar(seminar2);
			enrollmentInvitedSpeaker.setUser(invitedSpeaker);
			enrollmentInvitedSpeaker.setTipeEnrollment("invited speaker");

			invitedSpeaker.getListEnrollment().add(enrollmentInvitedSpeaker);

			enrollmentDb.save(enrollmentInvitedSpeaker);

		};
	}
}
