package com.MEDIS.security;

import java.io.IOException;
import java.time.LocalDateTime;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.MEDIS.model.BannedStatusModel;
import com.MEDIS.model.Id.BannedStatusId;
import com.MEDIS.service.BannedStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.service.UserService;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

public class BannedStatusFilter extends OncePerRequestFilter{
    @Autowired
    private UserService userService;

    @Autowired
    private BannedStatusService bannedStatusService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // Get the authenticated user from the session
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
            String username = user.getUsername();
//            System.out.println("ini username:"+username);
            UserModel userCheck = userService.getUserByEmail(username);

            // Check the user's banned status
            if (userCheck.getBannedStatus().equals("Banned")) {
                // User is banned, invalidate the session and redirect to login page
                if(userCheck.getBannedStatusModel().getMasaBerlaku().isBefore(LocalDateTime.now())){
                    BannedStatusModel bannedStatusModel = userCheck.getBannedStatusModel();
                    bannedStatusModel.setUser(null);
                    bannedStatusModel = bannedStatusService.saveBannedStatusModel(bannedStatusModel);
                    System.out.println(bannedStatusModel.getUser());

                    userCheck.setBannedStatusModel(null);
                    userService.saveUserNoEncrpyt(userCheck);
                    httpResponse.sendRedirect("/");
                    return;
                }
                httpRequest.getSession().invalidate();
                httpResponse.sendRedirect("/login?error=banned");
                return;
            }
        }

        filterChain.doFilter(request, response);
    }
}

