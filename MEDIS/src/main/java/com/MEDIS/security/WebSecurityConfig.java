package com.MEDIS.security;


import com.MEDIS.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    public BannedStatusFilter bannedStatusFilter(){
        return new BannedStatusFilter();
    }

    @Bean
    public ExpirationDateFilter expirationDateFilter(){
        return new ExpirationDateFilter();
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http
                .csrf()
                .ignoringAntMatchers("/")
                .ignoringAntMatchers("/sendMail")
                .ignoringAntMatchers("/forgot-password/**")
                .ignoringAntMatchers("/payments/handler")
                .and()
                .authorizeRequests()
                .antMatchers("/assets/**", "/sertifikat/**", "/faq", "/faq/**", "/error").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/signup").permitAll()
                .antMatchers("/test/**").permitAll()
                .antMatchers("/payments/handler").permitAll()
                .antMatchers("/sendMail").permitAll()
                .antMatchers("/forgot-password/**").permitAll()
                .antMatchers("/").permitAll()

                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .antMatchers("/penyelenggara/**").hasAuthority("PENYELENGGARA")
                .antMatchers("/invited-speaker/**").hasAuthority("INVITED SPEAKER")
                .antMatchers("/dokter/**").hasAuthority("DOKTER")

                .antMatchers("/seminars/add/**").hasAuthority("PENYELENGGARA")
                .antMatchers("/seminars/update/**").hasAuthority("PENYELENGGARA")
                .antMatchers("/seminars/upload-sertifikat/**").hasAuthority("PENYELENGGARA")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login").permitAll();

        http.addFilterBefore(bannedStatusFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(expirationDateFilter(), BannedStatusFilter.class);

        return http.build();

    }

    public static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
    }
}
