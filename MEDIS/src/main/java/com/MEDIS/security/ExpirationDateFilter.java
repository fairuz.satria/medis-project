package com.MEDIS.security;

import com.MEDIS.model.user.InvitedSpeakerModel;
import com.MEDIS.model.user.UserModel;
import com.MEDIS.service.InvitedSpeakerService;
import com.MEDIS.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

public class ExpirationDateFilter extends OncePerRequestFilter{
    @Autowired
    private InvitedSpeakerService invitedSpeakerService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // Get the authenticated user from the session
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
            String username = user.getUsername();
//            System.out.println("ini username:"+username);
            InvitedSpeakerModel userCheck = invitedSpeakerService.getByEmail(username);

            // Check the user's banned status
            if (userCheck != null && userCheck.getMasaBerlaku().isBefore(LocalDateTime.now())) {
                // User is banned, invalidate the session and redirect to login page
                httpRequest.getSession().invalidate();
                httpResponse.sendRedirect("/login?error=account-expired");
                return;
            }
        }

        filterChain.doFilter(request, response);
    }
}

