package com.MEDIS.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

import com.MEDIS.model.Id.BannedStatusId;
import com.MEDIS.model.user.UserModel;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "banned_status")
public class BannedStatusModel {
    @EmbeddedId
    private BannedStatusId bannedStatusId = new BannedStatusId();

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    // @JoinColumn(name = "user_id")
    private UserModel user;

    @Column(nullable = false)
    @NotNull
    private String jenisPelanggaran;

    @Column(nullable = false)
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime masaBerlaku;

    public void setUser(UserModel user) {
        if(user != null){
            this.user = user;
            bannedStatusId.setUserId(user.getUuid());
        }else {
            this.user = null;
        }
    }
}

