package com.MEDIS.model.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

import com.MEDIS.model.webinar.TransaksiModel;
import org.springframework.format.annotation.DateTimeFormat;

@Setter
@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "dokter")
public class DokterModel extends UserModel {

    @NotNull
    @Column
    private String nomorTelefon;

    @NotNull
    @Column
    private String institusi;

    @NotNull
    @Column
    private String pekerjaan;

    @Column
    private String spesialisasi;

    @Column
    private String kotaKabupaten;

    @Column
    private String jenisKelamin;


    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

//    @NotNull
//    @Column(name="str", nullable = false)
//    private String str;

}
