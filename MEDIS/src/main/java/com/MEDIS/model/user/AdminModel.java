package com.MEDIS.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

@Setter
@Getter
//@AllArgsConstructor
//@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "admin")
public class AdminModel extends UserModel {

}
