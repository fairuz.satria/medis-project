package com.MEDIS.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

import com.MEDIS.model.request.RequestInvitedSpeaker;
import org.springframework.format.annotation.DateTimeFormat;

@Setter
@Getter
//@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "invited_speaker")
public class InvitedSpeakerModel extends UserModel{

    
    @OneToOne(mappedBy = "user")
    // @JoinColumn(name = "address_id", referencedColumnName = "id")
    private RequestInvitedSpeaker request;

    @Column(nullable = false)
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime masaBerlaku;
}
