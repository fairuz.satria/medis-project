package com.MEDIS.model.webinar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

import com.MEDIS.model.Id.SertifikatId;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sertifikat")
public class SertifikatModel {
    // @Id
    // private String sertifikatId;

    @EmbeddedId
    private SertifikatId id = new SertifikatId();

    @Column(nullable = false)
    @NotNull
    private String sertifikatUrl;

    @Column
    @NotNull
    private LocalDate createdAt = LocalDate.now();


    @Column
    @NotNull
    private LocalDate expiredAt = LocalDate.now().plusYears(5);

    @MapsId("enrollmentId")
    @OneToOne(cascade = CascadeType.ALL)
    private EnrollmentModel enrollment;
}
