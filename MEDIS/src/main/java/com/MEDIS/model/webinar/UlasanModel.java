package com.MEDIS.model.webinar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import jakarta.persistence.*;
//import jakarta.validation.constraints.NotNull;

import com.MEDIS.model.Id.UlasanId;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ulasan")
public class UlasanModel implements Serializable {
//     @Id
//     private String ulasanId;

    @EmbeddedId
    private UlasanId id = new UlasanId();

//    @MapsId("enrollmentId")
//    @OneToOne(cascade = CascadeType.ALL)
//    private EnrollmentModel enrollment;
}
