package com.MEDIS.model.Id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.UUID;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BannedStatusId implements Serializable {
    @Column(length = 50)
    private String userId;

    @Column(length = 50)
    private String bannedId = UUID.randomUUID().toString();


}
