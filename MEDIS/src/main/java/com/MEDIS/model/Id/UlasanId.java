package com.MEDIS.model.Id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class UlasanId  implements Serializable{
    @Embedded
    private EnrollmentId enrollmentId;

    @Column(length = 50)
    private String ulasanId = UUID.randomUUID().toString();
}
