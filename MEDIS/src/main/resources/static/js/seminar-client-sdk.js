window.addEventListener('DOMContentLoaded', function (event) {
    console.log('DOM fully loaded and parsed');
    websdkready(sdkKey, signature, meetingNumber, passWord, userName, email, zak, leaveUrl);
});

function websdkready(sdkKey, signature, meetingNumber, passWord, userName, email, zak, leaveUrl) {

// loads WebAssembly assets
    ZoomMtg.preLoadWasm()
    ZoomMtg.prepareWebSDK()
// loads language files, also passes any error messages to the ui
    ZoomMtg.setZoomJSLib('https://source.zoom.us/2.10.1/lib', '/av')

    const zoomMeetingSDK = document.getElementById('zmmtg-root')

    console.log(sdkKey, signature, meetingNumber, passWord, userName, email, zak, leaveUrl)

    ZoomMtg.init({
        leaveUrl: leaveUrl, // https://example.com/thanks-for-joining
        success: (success) => {
            ZoomMtg.i18n.load('en-US');
            ZoomMtg.i18n.reload('en-US');
            ZoomMtg.join({
                sdkKey: sdkKey,
                signature: signature, // role in SDK signature needs to be 0
                meetingNumber: meetingNumber,
                passWord: passWord,
                userName: userName,
                userEmail: email,
                zak: zak,
                success: (success) => {
                    console.log("success " + success)
                },
                error: (error) => {
                    console.log("error1 " + error)
                }
            })
        },
        error: (error) => {
            console.log("error2 " + error)
        }
    })
}
