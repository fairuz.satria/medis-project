

// menghandle setiap card-faq-closed
document.querySelectorAll('.card-faq-closed').forEach((cardClosed, index) => {
    // mendapatkan elemen pertanyaan, jawaban, dan ikon
    const pertanyaan = cardClosed.querySelector('.pertanyaan');
    const jawaban = cardClosed.nextElementSibling; // mendapatkan elemen jawaban setelah card-faq-closed
    const iconOpen = pertanyaan.querySelector('.icon-dropdown');
    const iconClose = jawaban.querySelector('.icon-dropup');

    // menambahkan event listener pada ikon buka
    iconOpen.addEventListener('click', function() {
        // menampilkan card-faq-opened yang sesuai dengan card-faq-closed saat ini
        const openedCard = document.querySelectorAll('.card-faq-opened')[index];
        openedCard.style.display = 'flex';

        // menyembunyikan card-faq-closed saat ini
        cardClosed.style.display = 'none';
    });

    // menambahkan event listener pada ikon tutup
    iconClose.addEventListener('click', function() {
        // menyembunyikan card-faq-opened yang sesuai dengan card-faq-closed saat ini
        const openedCard = document.querySelectorAll('.card-faq-opened')[index];
        openedCard.style.display = 'none';

        // menampilkan kembali card-faq-closed saat ini
        cardClosed.style.display = 'grid';
    });
});