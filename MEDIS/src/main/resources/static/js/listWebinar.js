const onSubmitSearch = (event, role) => {
    event.preventDefault()
    console.log(event.srcElement[0].value)
    console.log(type)
    // var $j = jQuery.noConflict();

    // const buttons = document.querySelectorAll('.pagination .page-link');
    //
    // buttons.forEach(function (link) {
    //     link.parentNode.classList.remove('active');
    // });
    // document.getElementById("page-1").parentNode.classList.add('active');

    $(function() {
        $('#containerCard').load(`/seminars/${role}/list-seminar/live/card-container.html?searchBy=${event.srcElement[0].value}&type=${type}&page=1`);
    });
    $(function() {
        $('#footer-page').load(`/seminars/footer.html?searchBy=${event.srcElement[0].value}&type=${type}&page=1`);
    });
}

const onClickPage = (event) => {
    const searchBy = document.getElementById("search").value
    const selectedButton = event.target
    const page = selectedButton.innerHTML

    const buttons = document.querySelectorAll('.pagination .page-link');

    buttons.forEach(function (link) {
        link.parentNode.classList.remove('active');
    });
    // Add the active class to the clicked pagination link
    selectedButton.parentNode.classList.add('active');

    console.log(event, searchBy, selectedButton, role, type, page)
    $(function() {
        $('#containerCard').load(`/seminars/${role}/list-seminar/live/card-container.html?searchBy=${searchBy}&type=${type}&page=${page}`);
    });
}

const onClickNextPage = (event, totalPages) => {
    const searchBy = document.getElementById("search").value
    const selectedButton = document.getElementsByClassName("page-item active")[0].children[0]
    let page = selectedButton.innerHTML

    if (page != totalPages) {
        page = String(Number(page) + 1)
    }

    const buttons = document.querySelectorAll('.pagination .page-link');

    buttons.forEach(function (link) {
        link.parentNode.classList.remove('active');
    });
    // Add the active class to the clicked pagination link
    document.getElementById(`page-${page}`).parentNode.classList.add('active');

    console.log(event, searchBy, selectedButton, role, type, page)
    $(function() {
        $('#containerCard').load(`/seminars/${role}/list-seminar/live/card-container.html?searchBy=${searchBy}&type=${type}&page=${page}`);
    });
}

const onClickPrevPage = (event, totalPages) => {
    const searchBy = document.getElementById("search").value
    const selectedButton = document.getElementsByClassName("page-item active")[0].children[0]
    let page = selectedButton.innerHTML

    if (page != 1) {
        page = String(Number(page) - 1)
    }

    const buttons = document.querySelectorAll('.pagination .page-link');

    buttons.forEach(function (link) {
        link.parentNode.classList.remove('active');
    });
    // Add the active class to the clicked pagination link
    document.getElementById(`page-${page}`).parentNode.classList.add('active');

    console.log(event, searchBy, selectedButton, role, type, page)
    $(function() {
        $('#containerCard').load(`/seminars/${role}/list-seminar/live/card-container.html?searchBy=${searchBy}&type=${type}&page=${page}`);
    });
}
