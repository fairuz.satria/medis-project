function joinMeeting(sdkKey, signature, meetingNumber, passWord, userName, email, zak, seminarId) {
    const client = ZoomMtgEmbedded.createClient()
    let meetingSDKElement = document.getElementById('meetingSDKElement')
    // console.log("test")
    // console.log(client)
    // console.log(meetingSDKElement)
    // console.log("----")

    client.init({ zoomAppRoot: meetingSDKElement, language: 'en-US' })

    console.log(sdkKey, signature, meetingNumber, passWord, userName, email, zak, seminarId)

    client.join({
        sdkKey: sdkKey,
        signature: signature, // role in SDK signature needs to be 0
        meetingNumber: meetingNumber,
        password: passWord,
        userName: userName,
        userEmail: email,
        zak: zak
    }).then((e) => {
        console.log("join success", e);
    }).catch((e) => {
        console.log("join error", e);
    });
}