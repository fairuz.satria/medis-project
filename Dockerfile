FROM openjdk:17-alpine
RUN apk add --no-cache fontconfig msttcorefonts-installer && \
    update-ms-fonts && \
    fc-cache -f
ENV JAVA_TOOL_OPTIONS="-Djava.awt.headless=true"
ARG JAR_FILE=MEDIS/build/libs/MEDIS-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
COPY MEDIS/storage-admin-key.json storage-admin-key.json
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]